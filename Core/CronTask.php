<?php

namespace Core;

require 'SysUtils.php';
use Core\SysUtils;

class CronTask
{
    public const FILE_PREFIX = '/etc/cron.d/uspd_';
    public $minutes = '3-59/30';
    public $hours = '*';
    public $days = '*';
    public $months = '*';
    public $weekdays = '*';
    public $userName = 'root';
    public $command = '';
    public $enabled = false;
    public $name;

    public function __construct(string $name = 'poll') 
    {
        $this->name = $name;
        $this->command = Config::getStatic('requestDataScript');
    }

    public static function read(string $name = 'poll', bool $needCreate): CronTask
    {           
        $fileName =  self::FILE_PREFIX.$name;
        if (file_exists($fileName)) {
            $cron = new static($name);
            $cron->enabled = true;
            $file = file($fileName)[0];
            if ($file[0] === "#") {
                $file = substr($file, 1);
                $cron->enabled = false;
            }
            preg_match("/(\*\s|\*+\/\d+\s|\d++-\d++\/\d+\s)(\*\s|\*+\/\d+\s|\d++-\d++\/\d+\s)(\*\s|\*+\/\d+\s|\d++-\d++\/\d+\s)(\*\s|\*+\/\d+\s|\d++-\d++\/\d+\s)(\*\s|\*+\/\d+\s|\d++-\d++\/\d+\s)(\S+)\s([\S\s]+)/", $file, $arr);
            if (empty($arr[6]) || empty($arr[7])) {
                return null;
            }
            array_walk($arr, function (&$value) {
                $value = trim($value);
            });
            $cron->minutes = $arr[1];
            $cron->hours = $arr[2];
            $cron->days = $arr[3];
            $cron->months = $arr[4];
            $cron->weekdays = $arr[5];
            $cron->userName = $arr[6];
            $cron->command = $arr[7];
            $cron->name = $name;
            return $cron;
        } elseif ($needCreate) {
            $cron = new static($name);
            $cron->createCronFile();
            $cron->write();
            return $cron;
        }
        return null;
    }

    public function write()
    {
        $toWrite = $this->enabled? "": "#";        
        $toWrite .= implode(" ", [$this->minutes, $this->hours, $this->days, $this->months, $this->weekdays, $this->userName, $this->command]);        
        SysUtils::writeToFile($this->getFileName(), $toWrite);
        SysUtils::restartService('cron');
    }
    
    public function getMinutes(): int
    {
        return explode("/", $this->minutes)[1];
    }
    
    public function getFileName(): string 
    {
        return self::FILE_PREFIX.$this->name;
    }
    
    protected function createCronFile()
    {
        exec("sudo touch ".$this->getFileName());        
    }
}

