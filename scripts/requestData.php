<?php
require __DIR__.'/../Core/GoRequestData.php';

use Core\GoRequestData;
use Core\HelpTo;
$isDebug = in_array("debug", $argv);
$go = new GoRequestData($isDebug);
$go->init();
$render = $go->run();
if (!$isDebug) {
    return HelpTo::render($render);
}
