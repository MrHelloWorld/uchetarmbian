<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

require_once 'Config.php';
require_once 'Helper.php';

use DateTime;
use Core\HelpTo;

/**
 * Description of Logger
 *
 * @author Serg
 */
class Logger
{
    private static function checkExists()
    {
        if (!file_exists(Config::getStatic('logPath'))) {
            $exe = Config::getStatic('logPath');
            exec("touch $exe");
        }
    }

    public static function truncateLogFile()
    {
        $logName = Config::getStatic('logPath');
        $logSize = filesize($logName);
        $maxSize =  Config::get('maxLogSize') * 1024;
        if ($logSize < $maxSize) {
            return;
        }
        self::writeLog("Log file is bigger than needed. Truncating...");
        copy($logName, $logName.".bak");
        $oldFile = fopen($logName.".bak", "r");
        $newFile = fopen($logName, "w");
        fseek($oldFile, $logSize - $maxSize);
        while (($char = fgetc($oldFile))!== false) {
            if ($char === "\n") {
                break;
            }
        }
        while (($data = fread($oldFile, 8192)) !== false) {
            fwrite($newFile, $data);
            if ($data == "") {
                break;
            }
        }
        fclose($newFile);
        fclose($oldFile);
        unlink($logName.".bak");
    }

    public static function writeLog($log, $type = 'DEBUG')
    {
        $debugLevel = Config::get('debugLevel');

        if ($debugLevel == 'NONE') {
            return;
        }
        if (($debugLevel === 'ERROR' && $type === 'ERROR') || $debugLevel === 'DEBUG') {
            $file = fopen(Config::getStatic('logPath'), "a");
            $dt = new DateTime();
            $line = $log != "" ? $dt->format("d.m.Y H:i:s") . ' ' . $log . "\n" : "\n";
            fputs($file, $line);
            fclose($file);
        }
    }

    public static function getSize()
    {
        self::checkExists();
        return sprintf("%5.2f", filesize(Config::getStatic('logPath')) / 1024);
    }

    public static function getRender()
    {
        $logFile = fopen(Config::getStatic('logPath'), 'r');
        $arr = [];
        $i = 0;       
        while (($line = fgets($logFile)) !== false) {
            $i++;
            $arr[$i]['1'] = '<div class="text-center"> <b>'.substr($line, 0, 19).'</b></div>';
            $arr[$i]['2'] = substr($line, 19);
        }
        $render = '<div class="container p-0">';
        $render .= '<h3 class="text-center"> Лог файл </h3>';
        $render .= '<div class = "text-center">';
        $render .= '<a href="index.php?action=clearLog" class="px-5 mb-4 btn btn-primary text-center justify-content-center">Очистить</a>';
        $render .= '</div">';

        $render .= @HelpTo::returnHTMLTable($arr);
        //$render .= '</div>';
        $render .= '<div class = "text-center">';
        $render .= '<a href="#top" class="px-5 my-4 btn btn-primary text-center justify-content-center">Наверх</a>';
        //$render .= '</div>';
        fclose($logFile);
        
        return $render;
    }
}
