<?php
require 'Controllers/LoginController.php';

$login = new LoginController($needLogin = false);
$login->init();
$login->process();        