<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author m
 */
require 'AbstractPageController.php';
require 'Core/Helper.php';
require 'Core/CronTask.php';
require 'Core/Logger.php';
require_once 'config/config.php';

use Core\SysUtils;
use Core\CronTask;
use Core\Database;
use Core\DBQuery;
use Core\Config;
use Core\Logger;
use Core\Request;

class IndexController extends AbstractPageController
{
    private $daemonPID;
    private $isDaemonActive;
    private $daemonScript;

    public function init()
    {
        parent::init();
        $this->daemonScript = Config::getStatic('daemonScript');
        $this->daemonPID = SysUtils::getPID($this->daemonScript);
        $this->isDaemonActive = SysUtils::isProcessRunning($this->daemonScript);
    }

    public function process()
    {
        $this->proceedActions();
        $pollTask = CronTask::read("poll", $needCreate = true);
        $logSize = Logger::getSize();
        $isDaemonActive = $this->isDaemonActive;
        $params = compact('pollTask', 'logSize', 'isDaemonActive');
        $this->render('views/index.view.php', $params);
    }

    protected function startDaemonAction()
    {
        Logger::writeLog("Старт службы сервера...", 'ERROR');
        if (!$this->isDaemonActive) {
            exec($this->daemonScript." > /dev/null &");
        }
        Request::redirect('index.php');
    }

    protected function stopDaemonAction()
    {
        Logger::writeLog("Остановки службы сервера.", 'ERROR');
        SysUtils::killProcess($this->daemonScript);
        Request::redirect('index.php');
    }

    protected function startPollAction()
    {
        Logger::writeLog("Запуск опросов.", 'ERROR');
        $pollTask = CronTask::read('poll', false);
        $pollTask->enabled = true;
        $pollTask->write();
        Request::redirect('index.php');
    }

    protected function stopPollAction()
    {
        Logger::writeLog("Остановка опросов.", 'ERROR');
        $pollTask = CronTask::read('poll', false);
        $pollTask->enabled = false;
        $pollTask->write();
        Request::redirect('index.php');
    }

    protected function vacuumAction()
    {
        Logger::writeLog("Сжатие БД.", 'ERROR');
        $query = new DBQuery(Database::get());
        $query->vacuum();
        Request::redirect('index.php');
    }

    protected function clearLogAction()
    {
        $file = fopen(Config::getStatic('logPath'), "w");
        fclose($file);
        Logger::writeLog("Очистка журнала", 'ERROR');
        Request::redirect('index.php');
    }

    protected function initDBAction()
    {
        Database::get(true);
        Logger::writeLog("Cброс настроек и удаление данных", 'ERROR');
        Request::redirect('index.php');
    }

    protected function clearDBAction()
    {
        $query = new DBQuery(Database::get());
        if (!$query->DBexists())
            Database::get(true);
        $query->sql("DELETE FROM pm130_data");
        Logger::writeLog("Удаление всех данных", 'ERROR');
        Request::redirect('index.php');
    }

    protected function requestDataAction()
    {
        if (Request::isSetGET('debug')) {
            exec(Config::getStatic('requestDataScript'). ' debug' ,$out);
            var_dump($out);
        } else {
            exec(Config::getStatic('requestDataScript')." > /dev/null &" );
            Request::redirect('index.php');
        }
    }

    protected function fixNTPTypeAction()
    {
        (new DBQuery(Database::get()))->sql('update config set type = '.Config::TYPE_STRING.' where key = \'NTPServer\'');
        Logger::writeLog("Адрес NTP сервера теперь может указыаться не только в виде IP адреса", 'ERROR');
        Request::redirect('index.php');
    }
}
