<?php
require 'partial/header.view.php';
?>

<script>
    $(function (){
        selectModbusType($('#modbusLocalType')[0])  
    });
</script>

<h2 class="text-center mb-4"> Параметры</h2>
<div class="row mb-4">
    <div class="col-1 "></div>
    <div class="col-2 mb-4 p-0 m-0">
        <ul class="nav flex-column nav-pills border-left border-top border-bottom" role="tablist">
            <li class="nav-item border-bottom">
                <a class="nav-link active hover <?= @$commonError ?>" data-toggle="tab" href="#tabCommon"> Общие </a>
            </li>
            <li class="nav-item border-bottom ">
                <a class="nav-link hover <?= @$modbusError ?>" data-toggle="tab" href="#tabModbus"> Modbus </a>
            </li>
            <li class="nav-item border-bottom">
                <a class="nav-link hover <?= @$pollError ?>" data-toggle="tab" href="#tabPoll"> Опрос </a>
            </li>
            <li class="nav-item border-bottom">
                <a class="nav-link hover <?= @$networkError ?>" data-toggle="tab" href="#tabNetwork"> Сеть </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link hover" data-toggle="tab" href="#tabSystem"> Система </a>
            </li>
        </ul>
    </div>

    <div class="col-8  border p-4">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tabCommon" role='tabpanel'>
                <form action="../settings.php" method="GET" class="">
                    <input type="hidden" name="action" value="updateCommon">
                    <?php foreach ($settings[0] as $col => $value): ?>
                        <?php $value = $value[0]; ?>
                        <div class="form-group row <?= @${$col} ?> mx-0">
                            <label class="col-form-label col-5" for="<?= $col ?>"><?= Core\Alias::get($col) ?>: </label>
                            <div class="col-6 ">
                                <?php if ($col === 'debugLevel'): ?>
                                    <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                                        <option <?= ($value == 'DEBUG') ? 'selected' : '' ?>>DEBUG</option>
                                        <option <?= ($value == 'ERROR') ? 'selected' : '' ?>>ERROR</option>
                                        <option <?= ($value == 'NONE') ? 'selected' : '' ?>>NONE</option>
                                    </select>
                                <?php else: ?>
                                    <input class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" value="<?= $value ?>">
                                <?php endif; ?>
                                <small class="alert-danger"><?= @${$col . '_msg'} ?></small>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Применить</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="tabModbus" role='tabpanel'>
                <div id="localModbusPanel">
                    <form action="../settings.php" method="GET" class="">
                        <input type="hidden" name="action" value="updateLocalModbus">
                        <?php
                            foreach ($settings[1] as $col => $value):
                            $value = $value[0];
                        ?>
                            <div class="form-group row <?= @${$col} ?> mx-0">
                                <label class="col-form-label col-5" for="<?= $col ?>"><?= Core\Alias::get($col) ?>: </label>
                                <div class="col-6">
                                    <?php if ($col === 'modbusLocalType') : ?>
                                    <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" onchange="selectModbusType(this)">
                                        <option value="0" <?= ($value == '0') ? 'selected' : '' ?>>Локальный</option>
                                        <option value="1" <?= ($value == '1') ? 'selected' : '' ?>>Внешний</option>
                                    </select>
                                    <?php elseif ($col === 'modbusBaudRate') : ?>
                                        <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                                            <option <?= ($value == '4800') ? 'selected' : '' ?>>4800</option>
                                            <option <?= ($value == '9600') ? 'selected' : '' ?>>9600</option>
                                            <option <?= ($value == '19200') ? 'selected' : '' ?>>19200</option>
                                            <option <?= ($value == '28800') ? 'selected' : '' ?>>28800</option>
                                            <option <?= ($value == '38400') ? 'selected' : '' ?>>38400</option>
                                            <option <?= ($value == '57600') ? 'selected' : '' ?>>57600</option>
                                            <option <?= ($value == '115200') ? 'selected' : '' ?>>115200</option>
                                        </select>
                                    <?php elseif ($col === 'modbusNumBits') : ?>
                                        <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                                            <option <?= ($value == '8') ? 'selected' : '' ?>>8</option>
                                            <option <?= ($value == '7') ? 'selected' : '' ?>>7</option>
                                        </select>                                    
                                    <?php elseif ($col === 'modbusParity') : ?>
                                        <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                                            <option <?= ($value == 'None') ? 'selected' : '' ?>>None</option>
                                            <option <?= ($value == 'Even') ? 'selected' : '' ?>>Even</option>
                                            <option <?= ($value == 'Odd') ? 'selected' : '' ?>>Odd</option>
                                        </select>                                    
                                    <?php elseif ($col === 'modbusStopBits') : ?>
                                        <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                                            <option <?= ($value == '1') ? 'selected' : '' ?>>1</option>
                                            <option <?= ($value == '2') ? 'selected' : '' ?>>2</option>
                                        </select>                                    
                                    <?php else: ?>
                                        <input class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" value="<?= $value ?>">
                                    <?php endif; ?>
                                    <small class="alert-danger"><?= @${$col . '_msg'} ?></small>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Применить</button>
                        </div>
                    </form>
                </div>
                <div id="remoteModbusPanel">
                    <form action="../settings.php" method="GET" class="">
                        <input type="hidden" name="action" value="updateRemoteModbus">
                        <?php
                            foreach ($settings[2] as $col => $value):
                            $value = $value[0];
                        ?>
                            <div class="form-group row <?= @${$col} ?> mx-0">
                                <label class="col-form-label col-5" for="<?= $col ?>"><?= Core\Alias::get($col) ?>: </label>
                                <div class="col-6">
                                    <?php if ($col === 'modbusRemoteType') : ?>
                                        <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" onchange="selectModbusType(this)">
                                            <option value="0" <?= ($value == '0') ? 'selected' : '' ?>>Локальный</option>
                                            <option value="1" <?= ($value == '1') ? 'selected' : '' ?>>Внешний</option>
                                        </select>
                                    <?php elseif ($col === 'modbusBaudRate') : ?>
                                        <select class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>">
                                            <option <?= ($value == '4800') ? 'selected' : '' ?>>4800</option>
                                            <option <?= ($value == '9600') ? 'selected' : '' ?>>9600</option>
                                            <option <?= ($value == '19200') ? 'selected' : '' ?>>19200</option>
                                            <option <?= ($value == '28800') ? 'selected' : '' ?>>28800</option>
                                            <option <?= ($value == '38400') ? 'selected' : '' ?>>38400</option>
                                            <option <?= ($value == '57600') ? 'selected' : '' ?>>57600</option>
                                            <option <?= ($value == '115200') ? 'selected' : '' ?>>115200</option>
                                        </select>
                                    <?php else: ?>
                                        <input class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" value="<?= $value ?>">
                                    <?php endif; ?>
                                    <small class="alert-danger"><?= @${$col . '_msg'} ?></small>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Применить</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="tabPoll" role='tabpanel'>
                <form action="../settings.php" method="GET" class="">
                    <input type="hidden" name="action" value="updatePoll">
                    <?php foreach ($settings[3] as $col => $value): ?>
                        <?php $value = $value[0]; ?>
                        <div class="form-group row <?= @${$col} ?> mx-0">
                            <label class="col-form-label col-5" for="<?= $col ?>"><?= Core\Alias::get($col) ?>: </label>
                            <div class="col-6 ">
                                <input class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" value="<?= $value ?>">
                                <small class="alert-danger"><?= @${$col . '_msg'} ?></small>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Применить</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="tabNetwork" role='tabpanel'>
                <form action="../settings.php" method="GET" class="">
                    <input type="hidden" name="action" value="updateNetwork">
                    <?php foreach ($settings[4] as $col => $value): ?>
                        <?php $value = $value[0]; ?>
                        <div class="form-group row <?= @${$col} ?> mx-0">
                            <label class="col-form-label col-5" for="<?= $col ?>"><?= Core\Alias::get($col) ?>: </label>
                            <div class="col-6 ">
                                <input class="form-control" type="text" id ="<?= $col ?>" name="<?= $col ?>" value="<?= $value ?>">
                                <small class="alert-danger"><?= @${$col . '_msg'} ?></small>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Применить</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="tabSystem" role='tabpanel'>
                <h4 class="text-center mb-4"> Управление</h4>
                <div class="text-center">
                    <button type="button" data-toggle="modal" data-target="#rebootModal" class="btn btn-primary px-4" onclick='showModal("Перезагрузить?", "settings.php?action=reboot")'>Перезагрузить</button>
                    <button type="button" data-toggle="modal" data-target="#rebootModal" class="btn btn-primary px-5" onclick='showModal("Сбросить к заводским установкам и стереть все данные?", "index.php?action=initDB")'>Сбросить</button>
                </div>
            </div>

        </div>
    </div>
</div>

<?php
require 'partial/footer.view.php';
