<?php

namespace Core;

use Core\Database;
use PDO;

class DBQuery
{
    /**
     *
     * @var PDO
     */
    private $pdo;

    /* @var $db Database*/
    public function __construct($db)
    {
        $this->pdo = $db->pdo;
    }

    public function selectAll($table, $id = null)
    {
        if ($id == null)
            $sql = "SELECT * FROM {$table} LIMIT 500";
        else
            $sql = sprintf("SELECT * FROM %s WHERE %s = %s LIMIT 500", $table, array_keys($id)[0], array_values($id)[0]);
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectLast($table, $column, $minValue)
    {
        $stm = $this->pdo->prepare("SELECT * FROM {$table} WHERE $column > $minValue");
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param type $table
     * @param type $where
     *
     *['idColumn']  Column for ID
     *['id']        ID
     *['column']    Column
     *['min']       Min value for Column
     *['max']       Max value for Column
     *['limit']     LIMIT
     *['offset']    OFFSET
     * @return array
     */

    public function selectBetweenForID($table, $where)
    {
        $sql = sprintf("SELECT * FROM $table WHERE %s=%s AND %s>=%s AND %s<=%s",
                $where['idColumn'],
                $where['id'],
                $where['column'],
                $where['min'],
                $where['column'],
                $where['max']);
        $sql .= isset($where['limit'])? " LIMIT {$where['limit']}": "";
        $sql .= isset($where['offset'])? " OFFSET {$where['offset']}": "";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function selectBetweenForIDGen($table, $where)
    {
        $sql = sprintf("SELECT * FROM $table WHERE %s=%s AND %s>=%s AND %s<=%s",
                $where['idColumn'],
                $where['id'],
                $where['column'],
                $where['min'],
                $where['column'],
                $where['max']);
        $sql .= isset($where['limit'])? " LIMIT {$where['limit']}": "";
        $sql .= isset($where['offset'])? " OFFSET {$where['offset']}": "";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC))
            yield $row;
    }
    
    public function selectDataDiffBetweenForID($counterId, $beginStamp, $endStamp, $limit, $offset)
    {
        ini_set('max_execution_time', 900);
        $sql = "SELECT 
                    id,
                    pm130_id,
                    value1 - (SELECT value1 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value1',
                    value2 - (SELECT value2 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value2',
                    value3 - (SELECT value3 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value3',
                    value4 - (SELECT value4 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value4',
                    unix_timestamp,
                    string_timestamp
                FROM
                    pm130_data as cur
                WHERE
                    unix_timestamp > $beginStamp AND 
                    unix_timestamp <= $endStamp AND
                    pm130_id = $counterId
                LIMIT $limit OFFSET $offset";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function selectDataDiffBetweenForIDGen($counterId, $beginStamp, $endStamp)
    {
        ini_set('max_execution_time', 900);
        $sql = "SELECT 
                    id,
                    pm130_id,
                    value1 - (SELECT value1 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value1',
                    value2 - (SELECT value2 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value2',
                    value3 - (SELECT value3 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value3',
                    value4 - (SELECT value4 FROM pm130_data as prev WHERE cur.unix_timestamp > prev.unix_timestamp AND pm130_id = $counterId ORDER BY unix_timestamp DESC LIMIT 1) as 'value4',
                    unix_timestamp,
                    string_timestamp
                FROM
                    pm130_data as cur
                WHERE
                    unix_timestamp > $beginStamp AND 
                    unix_timestamp <= $endStamp AND
                    pm130_id = $counterId";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC))
            yield $row;
    }

    public function selectLastForID($table, $column, $minValue, $id, $idColumn='id' )
    {
        $stm = $this->pdo->prepare("SELECT * FROM {$table} WHERE $column > $minValue AND $idColumn=$id");
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function numRows($table)
    {
        $stm = $this->pdo->prepare("SELECT COUNT(*) as count from $table");
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC)[0]['count'];
    }

    public function numRowsBetweenForID($table, $where)
    {
        $sql = sprintf("SELECT COUNT(*) as count FROM $table WHERE %s=%s AND %s>=%s AND %s<=%s",
            $where['idColumn'],
            $where['id'],
            $where['column'],
            $where['min'],
            $where['column'],
            $where['max']);
        //echo $sql;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC)[0]['count'];
    }

    public function selectMax($table, $column, $whereEqual)
    {
        $sql = sprintf("SELECT * FROM $table WHERE %s=%s ORDER BY {$column} DESC LIMIT 1",
                        array_keys($whereEqual)[0], array_values($whereEqual)[0]);
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $ret = $stm->fetchAll(PDO::FETCH_ASSOC);

        return count($ret)>0? $ret[0][$column]: 0;
    }

    //Used for grep data for server responce
    public function selectDataForModbusID($modbusID, $startTime, &$retNumRows)
    {        
        $counterID = $this->pdo->query("SELECT id FROM pm130_counters WHERE device_id = $modbusID")->fetch(PDO::FETCH_ASSOC);
        if ($counterID == false)
        {
            $retNumRows = 0;
            return false;
        }
        //TO DO set limit
        $limit = 150;//\Core\PM130DataResponsePacket::MAX_SEND_DATA;
        $sql = "SELECT * FROM pm130_data WHERE unix_timestamp > $startTime AND pm130_id = {$counterID['id']} ORDER By unix_timestamp LIMIT $limit";
//        echo $sql."\n";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        $ret = $stm->fetchAll(PDO::FETCH_ASSOC);
        $retNumRows = count($ret);
        return $ret;
    }

    public function insert($table, $values)
    {
        $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)",
                        $table,
                        implode(", ", array_keys($values)),
                        implode(" ,:", array_keys($values))
                    );

        $stm = $this->pdo->prepare($sql);
        $stm->execute($values);
    }

    public function update($table, $id, $row, $value)
    {
        $this->pdo->exec("UPDATE {$table} SET {$row} = '{$value}' WHERE id='{$id}'");
    }

    public function deleteByID($table, $id)
    {
        $this->pdo->exec("DELETE FROM {$table} WHERE id='{$id}'");
    }

    public function deleteByColumn($table, $column, $value)
    {
        $this->pdo->exec("DELETE FROM {$table} WHERE {$column}='{$value}'");
    }

    public function deleteLessThan($table, $column, $minValue)
    {
        $sql = "DELETE FROM {$table} WHERE {$column}<'{$minValue}'";
        //echo $sql;
        $this->pdo->exec("DELETE FROM {$table} WHERE {$column}<'{$minValue}'");
    }
    
    public function DBexists()
    {
        return $this->numRows('sqlite_master');
    }

    public function vacuum()
    {
        $this->pdo->exec("VACUUM");
    }

    public function sql($sql)
    {
        $this->pdo->exec($sql);
    }
}