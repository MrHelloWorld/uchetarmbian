<?php

namespace Core;

use DateTime;
use DateTimeZone;

class HelpTo {

    public static function echoHex($val) {
        //echo '<pre>';
        $res = unpack("H*", $val);

        $splt = str_split($res[1], 2);
        foreach ($splt as $el) {
            echo $el . '_';
        }
        echo "<br>\n";
        //echo '</pre>';
    }

    public static function echoStrHex($str, $val) {
        //echo '<pre>';
        $res = unpack("H*", $val);

        $splt = str_split($res[1], 2);
        echo $str . ': ';
        foreach ($splt as $el) {
            echo $el . '_';
        }
        echo "<br>\n";
        //echo '</pre>';
    }

    public static function echoHexArray($val, $delimeter = '_') {
        echo '<pre>';

        $splt = str_split($val, 2);

        foreach ($splt as $el) {
            echo $el . $delimeter;
        }

        echo '<br>';
        echo '</pre>';
    }

    public static function echoHTMLTable1($table, $deleteScript = null) {
        
        if (sizeof($table) == 0)
        {
            echo '<h5 class="text-center">Таблица пуста</h5>';
            return;
        }
               
        echo "<table class = 'table table-bordered table-sm'>";
            echo "<thead class='thead-light yes'>";
                echo '<tr>';
                    foreach ($table[0] as $key => $value) {
                        echo "<th> {$key}</th>";
                    }                                        
                    echo $deleteScript? '<th class="text-center" style="width: 5%">X</th>': '';                    
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                foreach ($table as $val) {
                    echo '<tr>';
                    foreach ($val as $key => $value) {
                        echo "<td class=\"align-middle\"> {$value} </td>";
                    }
                    echo $deleteScript? '
                            <td class="align-top">
                                <button type="button" class="btn close">
                                    <a class="nav-link" href="'.$deleteScript.'?action=delete&id='.$val['id'].'">&times;
                                    </a>                                    
                                </button>
                            </td>' : '';
                    echo '</tr>';
                }
            echo '</tbody>';
        echo '</table>';
    }
    
    public static function echoHTMLTable($table, $deleteScript = null, $editArr=null)
    {    
        $idRow = 'id';
        $editable = false;
        if ($editArr != null)
        {
            //$idRow = $editArr['id'];
            $editableRows = $editArr['rows'];
            $editScript = $editArr['script'];
            $editable = true;
        }
        
        if (sizeof($table) == 0)
        {
            echo '<h5 class="text-center">Таблица пуста</h5>';
            return;
        }
               
        echo '<table class = "table table-bordered table-sm">';
            echo '<thead class="thead-light">';
                echo '<tr>';
                    foreach ($table[0] as $key => $value) {
                        echo "<th> {$key}</th>";
                    }                                        
                    echo $deleteScript? '<th class="text-center" style="width: 5%">X</th>': '';                    
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
                foreach ($table as $val) {
                    echo '<tr>';
                    foreach ($val as $key => $value) 
                    {                        
                        if ($editable)
                        {
                            echo "<td 
                                    class='align-middle' 
                                    data-id='$val[$idRow]' 
                                    data-row='$key' >";
                                                        
                            if (in_array($key, $editableRows))
                            {
                                echo "<a href='#' onclick=\"showEditBox('$editScript', this)\">{$value}</a>";                                
                            }
                            else
                                echo $value;
                            echo "</td>";
                        }
                        else
                            echo "<td class='align-middle'>{$value} </td>";
                                
                    }
                    echo $deleteScript? '
                            <td class="align-top">
                                <button type="button" class="btn close">
                                    <a class="nav-link" href="'.$deleteScript.'?action=delete&id='.$val['id'].'">&times;
                                    </a>                                    
                                </button>
                            </td>' : '';
                    echo '</tr>';
                }
            echo '</tbody>';
        echo '</table>';
    }
    
    public static function returnHTMLTable($table, $deleteScript = null) 
    {
        $ret = '';
                
        if (sizeof($table) == 0)
        {
            $ret .= '<h5 class="text-center">Таблица пуста</h5>';
            return $ret;
        }
               
        $ret .= '<table class = "table table-bordered table-sm">';
            $ret .= '<thead class="thead-light">';
                $ret .= '<tr>';
                    foreach ($table[0] as $key => $value) {
                        $ret .= "<th> {$key}</th>";
                    }                                        
                    $ret .= $deleteScript? '<th class="text-center" style="width: 5%">X</th>': '';                    
                $ret .= '</tr>';
            $ret .= '</thead>';
            $ret .= '<tbody>';
                foreach ($table as $val) {
                    $ret .= '<tr>';
                    foreach ($val as $key => $value) {
                        $ret .= "<td class=\"align-middle\"> {$value} </td>";
                    }
                    $ret .= $deleteScript? '
                            <td class="align-top">
                                <button type="button" class="btn close">
                                    <a class="nav-link" href="'.$deleteScript.'?action=delete&id='.$val['id'].'">&times;
                                    </a>                                    
                                </button>
                            </td>' : '';
                    $ret .= '</tr>';
                }
            $ret .= '</tbody>';
        $ret .= '</table>';
        
        return $ret;
    }
    
    public static function render($what)
    {        
        require 'views/partial/header.view.php';          
        echo $what;
        require 'views/partial/footer.view.php';
    }
    
    public static function printUnixTime($unxiTime)
    {
        $dt = new DateTime();
        $tz = new DateTimeZone('UTC');
        $dt->setTimestamp($unxiTime);
        $dt->setTimezone($tz);
        return $dt->format('Y-m-d H:i:s');
    }
}
    