<?php

namespace Core;

require 'PM130DataResponsePacket.php';
require 'RecievedPacket.php';
require '';

//date_default_timezone_set('UTC');
set_time_limit(0);

use Core\PM130DataResponsePacket;
use Core\Config;
use Core\HelpTo;
use Core\RecievedPacket;
use DateTime;

class Daemon
{
    public $localIP;
    public $localPort;
    public $timePort = 1115;
    public $socket = null;
    public $timeSocket = null;
    public $timeRecievedTime = 0;

    public function __construct()
    {
        $this->localIP = exec('uci get network.wan.ipaddr');
        $this->localPort = intval(Config::get('listenPort'));
        $this->socket = null;
    }

    public function initSocket()
    {
        if (Config::getStatic('timeSource') == 'NTP') {
            return true;
        }
        $this->timeSocket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        if (!$this->timeSocket) {
            Logger::writeLog("Error creating time daemon socket!", 'ERROR');
            return false;
        }
        if (!socket_set_option($this->timeSocket, SOL_SOCKET, SO_BROADCAST, 1)) {
            Logger::writeLog("Error setting time socket daemon option!", 'ERROR');
            return false;
        }
        if (!socket_bind($this->timeSocket, '0.0.0.0', $this->timePort)) {
            Logger::writeLog("Can not bind recieve time socket in daemon!", 'ERROR');
            return false;
        }
        return true;
    }

    public function recieveTimePacket()
    {
        $ret =[];
        $timeBuf = '';
        $fromIP = '';
        $fromPort = 0;

        socket_recvfrom($this->timeSocket, $timeBuf, 512, MSG_DONTWAIT, $fromIP, $fromPort);
        $t = strlen($timeBuf);
        if ($t > 0 ) {
            $ret['type'] = 'time';
            $ret['data'] = $timeBuf;
            return $ret;
        }
        time_nanosleep(0, 50000000);
        return null;
    }

    public function run()
    {
        if (!$this->socket) {
            Logger::writeLog("Operation on null socket", 'ERROR');
            return;
        }

        $serverIP = Config::get('serverIP');
        $serverPort = Config::get('serverPort');
        $useNTP = (Config::getStatic('timeSource') == 'NTP')? true: false;
        while (true)
        {            
            echo '.';
            $recievedData = $this->recieveDataPacket();
            if (!$useNTP && $recievedData == null) {
                $recievedData = $this->recieveTimePacket();
            }
            if ($recievedData == null) {
                continue;
            }
            $packet = new RecievedPacket($recievedData);
            //$packet->printPacket();
            if (!$packet->decode()) {
                Logger::writeLog("Wrong packet recieved. Skipping...", 'ERROR');
                continue;
            }
            if($packet->header == RecievedPacket::PACKET_GET_METTER_DATA4_FROM) {
                $response = new PM130DataResponsePacket($serverIP, $serverPort);
                $ret = $response->create($packet->data['station_id'], $packet->data['modbus_id'], $packet->data['unix_timestamp']);
                if ($ret == true) {                                        
                    $response->send($this->socket);                    
                }
                //else
                //    echo "Nothing To Send\n";
            }
            else
            if($packet->header == RecievedPacket::PACKET_BROADCAST_TIME) {
                $dt = DateTime::createFromFormat("d-m-Y-H-i-s", "{$packet->data['day']}-{$packet->data['month']}-{$packet->data['year']}-".
                                                                 "{$packet->data['hour']}-{$packet->data['minute']}-{$packet->data['second']}");
                //print_r(DateTime::getLastErrors());
                $curTime = new DateTime();
                $dif = $dt->diff($curTime);
                print_r($packet->data);
                print_r($dif);
                //print_r($curTime);
                $total = $dif->y + $dif->m + $dif->d + $dif->h + $dif->i;
                if ($total > 0 || $dif->s > 5) {
                    //print_r($dif);
                    $format = $dt->format("Y-m-d H:i:s");
                    $exec = "date -s '".$format."'";
                    exec($exec);
                    Logger::writeLog ("Correcting time: from ".$curTime->format("Y-m-d H:i:s"). " to $format", 'ERROR');
                    //echo "Correcting time: from ".$curTime->format("Y-m-d H:i:s"). " to $format";
                }
                unset($curTime);
                unset($dt);
                unset($dif);
            }
            unset($packet);
        }
    }
}
