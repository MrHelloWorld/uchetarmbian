<?php

namespace Core\Modbus;

require_once __DIR__.'/../Helper.php';

    /**
     * MODBUS SEND FRAME:
     *                  Header                                  PDU             Starting Register     Number of regs to read
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE           START_REG           REGS_NUM
     *      2b              2b        2b       1b               1b                     2b                  2b
     *
     *
     * MODBUS RECIEVE FRAME:
     *                  Header                                  PDU
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE   REC_LENGHT       DATA
     *      2b              2b        2b       1b               1b            2b         REC_LENGHT
     */

class ModbusRTUPacket
{
    const PROTOCOL_ID                       = 0x00;
    const FNC_READ_HOLDING_REGISTERS        = 0x03;
    const FNC_WRITE_SINGLE_REGISTER         = 0x06;
    const FNC_WRITE_MULTIPLE_REGISTERS      = 0x10;

    protected $deviceID = 0;
    protected $functionCode = 0;
    protected $recievedData = "";
    protected $recievedDataLenght = 0;    
    protected $startAddr = 0;
    protected $lenght = 0;
    protected $rawData = null;

    public function __construct() 
    {
    }
    
    public function isValid(): bool
    {
        return  1<<8& $this->functionCode? false: true;
    }

    public function initToReadHoldingRegisters(int $deviceID, int $startAddr, int $lenght)
    {
        $this->deviceID = $deviceID;
        $this->functionCode = self::FNC_READ_HOLDING_REGISTERS;
        $this->startAddr = $startAddr;
        $this->lenght = $lenght;

        $this->rawData = pack(
            "CCnn",
            $deviceID,
            self::FNC_READ_HOLDING_REGISTERS,
            $startAddr,
            $lenght
            );
    }

    public function initToWriteSingleRegister(int $deviceID, int $startAddr, int $value)
    {
        $this->deviceID = $deviceID;
        $this->functionCode = self::FNC_READ_HOLDING_REGISTERS;
        $this->startAddr = $startAddr;
        $this->lenght = $value;

        $this->rawData = pack(
                "CCnn",
                $deviceID,
                self::FNC_WRITE_SINGLE_REGISTER,
                $startAddr,
                $value
            );
    }

    public function initToWriteMultipleRegisters(int $deviceID, int $startAddr, array $values)
    {
        $this->deviceID = $deviceID;
        $this->functionCode = self::FNC_WRITE_MULTIPLE_REGISTERS;
        $this->startAddr = $startAddr;
        $this->lenght = count($values);

        $this->rawData = pack(
                "CCnnC",
                $deviceID,
                self::FNC_WRITE_MULTIPLE_REGISTERS,
                $startAddr,
                count($values),
                count($values)*2
            );
        foreach ($values as $element) {
            $this->rawData .= pack("n", $element);
        }
    }

    public function getRawData()
    {
        return $this->rawData;
    }
}
