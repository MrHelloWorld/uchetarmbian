<?php
require 'partial/header.view.php';
?>

<div class="container">
    <div class="row justify-content-center ">
        <div class="col-3 border rounded p-3 m-5">
            <form action="login.php" method="POST">
                <div class="form-group">
                    <label>Пользователь</label>
                    <input name="login" type="text" class="form-control <?= $loginFail? 'is-invalid' : ''?>" value="root">
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input name="password" type="password" class="form-control <?= $loginFail? 'is-invalid' : ''?>" value="">
                    <div class="invalid-feedback">
                        <?= $loginFail? 'Имя или пароль указаны неверно' : ''?>
                    </div>                    
                </div>
                <button type="submit" class="btn btn-block btn-primary">
                    Войти
                </button>
            </form>
        </div>
    </div>
</div>

<?php
require 'partial/footer.view.php';
?>