<?php

require './Controllers/SettingsController.php';

$settings = new SettingsController();
$settings->init();
$settings->process();
