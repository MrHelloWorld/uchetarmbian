function updateData(elem, event)
{
    const startDate = jQuery("input[name='startDate']")[0].valueAsNumber / 1000 +
                      jQuery("input[name='startTime']")[0].valueAsNumber / 1000;
    const endDate = jQuery("input[name='endDate']")[0].valueAsNumber / 1000 +
                    jQuery("input[name='endTime']")[0].valueAsNumber / 1000;
    const diff = jQuery("input[name='diff']")[0].checked? 1: 0;
    let newPath = elem;
    newPath.pathname = "/ajax/mettersData_ajax.php";

    if (elem.classList.contains('hover'))
    {
        jQuery(".hover").removeClass('active');
        elem.classList.add('active');
    }

    if (elem.type == 'submit')
    {
        window.location.href = window.link+"&download";
    }else
    {
        jQuery('#data').html(`<h1 class="text-center">Loading...</h1>`);
        window.link = newPath.href + "&start=" + startDate + "&end=" + endDate + "&diff=" + diff;
        jQuery.get(window.link, function(data, status)
        {
            jQuery('#data').html(data+`
                <div class="text-center">
                    <button class="btn btn-primary text-center" onclick="updateData(this, event)">Download as CSV file...</button>
                </div>`);
        });
        event.preventDefault();
    }
    return false;
}

function showEditBox(script, element)
{
    let id = element.parentElement.dataset['id'];
    let row = element.parentElement.dataset['row'];
    let value = element.parentElement.innerText;
    let modal = ''+
    '<div class="modal fade" id="editModal">'+
    '   <div class="modal-dialog">'+
    '      <div class="modal-content">'+
    '           <div class=modal-header>'+
    '               <h5>Изменение</h5>'+
    '               <button class="close" data-dismiss="modal">'+
    '                   <span>&times</span>'+
    '               </button>'+
    '           </div>'+
    '           <div class=modal-body>'+
    '               <div class="from-group">'+
    '                  <label class="">Новое значение</label>'+
    '                   <input name="newValue" class="form-control" type="text" value="'+value+'">'+
    '               </div>'+
    '           </div>'+
    '           <div class=modal-footer>'+
    '               <a class="btn btn-primary" id="saveButton" href="#" onclick="updateEditLink(this)">Сохарнить</a>'+
    '               <button class="btn btn-primary" data-dismiss="modal">Отмена</button>'+
    '           </div>'+
    '       </div>'+
    '   </div>'+
    '</div>';
    if (!jQuery("#editModal").length)
        jQuery("body").prepend(modal);
    jQuery("#editModal").modal();
    jQuery("#saveButton")[0].href = script+"?action=update&id="+id+"&row="+row+"&value=";
    jQuery("#editModal").on('hidden.bs.modal', function()
    {
        jQuery("#editModal").remove();
    });
}

function showModal(text, action)
{
    const modal = `<div class="modal" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <h4 class="modal-title">Подтверждение</h4>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <h6>${text}</h6>
                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <a href="${action}" class="btn btn-secondary px-4">OK</a>
                    <a href="#" data-dismiss="modal" class="btn btn-secondary px-2">Отмена</a>
                </div>
            </div>
        </div>
    </div>`;

    jQuery("body").append(modal);
    jQuery("#modal").modal();
    jQuery("#modal").on('hidden.bs.modal', function()
    {
        jQuery("#modal").remove();
    });
}

function updateEditLink(element)
{
    let value = jQuery("input[name='newValue']")[0].value;
    element.href += value;
}

function getTime()
{
    jQuery.get("ajax/api.php", {'action': 'getTime'}, function (data, status)
    {
        jQuery("#time").html("Время УСПД: "+data);
    });
}

function selectModbusType(element)
{
    if (element.selectedIndex === 0) {
        $('#localModbusPanel').show();
        $('#remoteModbusPanel').hide();
        $('#modbusLocalType')[0].selectedIndex = 0;
        $('#modbusRemoteType')[0].selectedIndex = 0;
    } else {
        $('#localModbusPanel').hide();
        $('#remoteModbusPanel').show();
        $('#modbusLocalType')[0].selectedIndex = 1;
        $('#modbusRemoteType')[0].selectedIndex = 1;
    }
}
