<?php require 'partial/header.view.php'; ?>

<div class="container">


    <div class="row border-top">
        <div class="col-2 border-right">
        <br>
            <div class="form-group mb-1">
                <label>От</label>
                <input type="date" name="startDate" class="form-control" value="<?=(new DateTime())->format("Y-m-d")?>">
            </div>
            <div class="form-group row pb-4 border-bottom">
                <label class="col-form-label col-4">Время</label>
                <div class="col-8 pl-2">
                    <input type="time" name="startTime" class="form-control" value="00:00">
                </div>
            </div>
            <div class="form-group mb-1">
                <label>До</label>
                <input type="date" name="endDate" class="form-control" value="<?=(new DateTime())->add(new DateInterval("P1D"))->format("Y-m-d")?>">
            </div>
            <div class="form-group row pb-4 border-bottom mb-2">
                <label class="col-form-label col-4">Время</label>
                <div class="col-8 pl-2">
                    <input type="time" name="endTime" class="form-control" value="00:00">
                </div>
            </div>
            <div class="form-group form-check mb-1">
                <input id="pri" type="checkbox" checked class="form-check-input" name="diff">
                <label for="pri">Приращения</label>
            </div>
            <div class="form-group border-bottom row"></div>
            <ul class="nav flex-column nav-pills nav-fill">
                <li class="nav-item border-top border-left border-right ">
                    <?php
                        foreach ($allCounters as $cnt)
                        {
                            if ($cnt['id'] == $selectedID)
                                echo '<a id="'.$cnt['id'].'" class="hover border-bottom nav-link active counter" href="pm130data.php?id='.$cnt['id'].'" onclick="updateData(this, event)">'.$cnt['name'].'</a>';
                            else
                                echo '<a id="'.$cnt['id'].'" class="hover border-bottom nav-link counter" href="pm130data.php?id='.$cnt['id'].'" onclick="updateData(this, event)">'.$cnt['name'].'</a>';
                        }
                    ?>
                </li>
            </ul>
            <br><br><br><br><br>
        </div>

        <div class="col">
            <!--<h3 class="text-center">Данные</h3>--><br>
            <div id="data">

            </div>
            <br>
        </div>
    </div>
</div>

<?php require 'partial/footer.view.php'; ?>