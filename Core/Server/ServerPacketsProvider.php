<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Server;

require 'ResponsePM130DataPacket.php';

/**
 * Description of ServerPacketsProvider
 *
 * @author Serg
 */
class ServerPacketsProvider 
{
    const PACKET_GET_METTER_DATA4_FROM = 'C5';
    const PACKET_BROADCAST_TIME = 'FF';    
    
    protected $rawData;
    protected $header;
    protected $debug;

    public function __construct($packet, $debug=false) 
    {
        $this->rawData = $packet;
        $this->header = substr($this->rawData, 0, 2);
        $this->debug = $debug;
    }
    /**   
     * @return AbstractResponsePacket Description
     */
    public function createResponsePacket()
    {
        switch ($this->header) {
            case self::PACKET_GET_METTER_DATA4_FROM:
                return new ResponsePM130DataPacket(substr($this->rawData, 2),$this->debug);
        }
        return false;
    }
}
