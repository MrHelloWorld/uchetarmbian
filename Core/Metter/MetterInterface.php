<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Metter;

/**
 *
 * @author Serg
 */
interface MetterInterface
{
    public function open(): bool;
    public function close();
    public function getTime();
    public function setTime($hour, $min, $sec);
    public function correctMetterTime();
    public function getA2R2($startTime = 0);
}
