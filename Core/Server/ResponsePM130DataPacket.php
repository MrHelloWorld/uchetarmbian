<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Server;

require 'AbstractResponsePacket.php';
//require __DIR__.'/../DBQuery.php';

use Core\Database;
use Core\DBQuery;
use Core\Logger;
use Core\HelpTo;

/**
 * Description of ResponsePM130DataPacket
 *
 * @author Serg
 */
class ResponsePM130DataPacket extends AbstractResponsePacket
{
    const MAX_DATA_TO_SEND = 150;

    protected $rawData;
    protected $stationID;
    protected $modbusID;
    protected $startTimestamp;
    protected $numDataToSend;
    protected $debug;
    protected $dataToSend;

    public function __construct($packet, $debug = false)
    {
        $this->rawData = $packet;
        $this->stationID = unpack("v", substr($this->rawData, 0, 2))[1];
        $this->modbusID = unpack("C", substr($this->rawData, 2, 3))[1];
        $this->startTimestamp = unpack("V", substr($this->rawData, 3, 7))[1];
        $this->debug = $debug;
        $this->dataToSend = [];
        $this->create();
    }

    public function create()
    {
        $query = new DBQuery(Database::get());
        
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
        //$dataRows = $query->selectDataForModbusID(1, $this->startTimestamp, $this->numDataToSend);
        $dataRows = $query->selectDataForModbusID($this->modbusID, $this->startTimestamp, $this->numDataToSend);
        if ($dataRows == false) {
            if ($this->debug) {
                echo "No data recived from DB for ModbusID: $this->modbusID from ".HelpTo::printUnixTime($this->startTimestamp)."\n";
            }
            return false;
        }
        $this->dataToSend = $dataRows;
        if ($this->debug) {
            echo "Request for data since ".HelpTo::printUnixTime($this->startTimestamp).". Ready to send {$this->numDataToSend} rows.\n" ;
            //No data recived from DB for ModbusID: $this->modbusID from".HelpTo::printUnixTime($this->startTimestamp);
        }

        /*foreach ($dataRows as $row)
        {
            $data4['unix_time'] = $val['unix_timestamp'];
            $data4['value1'] = $val['value1'];
            $data4['value2'] = $val['value2'];
            $data4['value3'] = $val['value3'];
            $data4['value4'] = $val['value4'];
            $this->dataToSend[] = $data4;
        }*/
        return true;
    }

    public function send($socket, $serverIP, $serverPort)
    {
        if ($this->numDataToSend == 0)
        {
            //Logger::writeLog("Nothing to send. Skipping request");
            if ($this->debug) {
                echo "Nothing to send. Skipping request\n";
            }
            $this->data = null;
            return;
        }

        $max = 0;
        if ($this->numDataToSend > self::MAX_DATA_TO_SEND) {
            $this->numDataToSend = self::MAX_DATA_TO_SEND;
        }
        $toSend = pack("vCv", $this->stationID, $this->modbusID, $this->numDataToSend);
        for ($i = 0; $i < $this->numDataToSend; $i++) {
            $val = $this->dataToSend[$i];
            $toSend .= pack("VVVVV",
                        $val['unix_timestamp'],
                        $val['value1'],
                        $val['value2'],
                        $val['value3'],
                        $val['value4']
            );
        }
        //var_dump($toSend);
        //die();
        $ret = socket_sendto($socket, $toSend, strlen($toSend), 0, $serverIP, $serverPort);
        if ($this->debug) {
            //HelpTo::echoHex($toSend);
            echo "$ret bytes was sent\n";
        }
        Logger::writeLog("$this->numDataToSend records was sent for Station: $this->stationID, ModbusID: $this->modbusID. Request stamp: $this->startTimestamp (".HelpTo::printUnixTime($this->startTimestamp).")");
        $this->data = null;
    }
}
