<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

/**
 * Description of SysUtils
 *
 * @author m
 */
class SysUtils
{
    public static function getPIDs($script): array
    {
        $out = [];
        $pids = [];
        exec("ps -ax | grep '$script' | grep -v grep | grep -v sh", $out);
        foreach ($out as $value) {
            array_push($pids, explode(" ", trim($value))[0] );
        }
        return $pids;
    }

    public static function getPID(string $script): int
    {
        return isset(self::getPIDs($script)[0])? self::getPIDs($script)[0]: 0;
    }

    public static function isProcessRunning(string $script): bool
    {
        return count(self::getPIDs($script)) >0 ? true : false;
    }

    public static function getLocalIP(): string
    {
        $out = [];
        exec("sudo nmcli connection show ".Config::getStatic('ethConnectionName')." | grep ipv4.addresses | awk '{print $2}'", $out);
        if (count($out) == 0) {
            echo "Error getting local IP address for [".Config::getStatic('ethConnectionName')."] connection<br>";
        }
        return explode("/", $out[0])[0];
    }

    public static function getLocalGateway(): string
    {
        $out = [];
        exec("sudo nmcli connection show ".Config::getStatic('ethConnectionName')." | grep ipv4.gateway | awk '{print $2}'", $out);
        return $out[0];
    }

    public static function getLocalMask(): int
    {
        $out = [];
        exec("sudo nmcli connection show ".Config::getStatic('ethConnectionName')." | grep ipv4.addresses | awk '{print $2}'", $out);
        return explode("/", $out[0])[1];
    }

    public static function calculateMask(int $numNonZeroes): string
    {
        $mask = [0, 0, 0, 0];
        for ($i=0; $i<$numNonZeroes; $i++) {
            $mask[$i/8] += 1<<(7-$i%8);
        }
        return implode(".", $mask);
    }

    public static function calculateNonZeroValuesInMask(string $mask): int
    {
        $numNonZeroes = 0;
        $maskArr = explode(".", $mask);
        for ($i=0; $i<32; $i++) {
            if ((int)($maskArr[(int)($i/8)]) & (1<<($i%8))) {
                $numNonZeroes++;
            }
        }
        return $numNonZeroes;
    }

    public static function getNTPServer(): string
    {
        $out = [];
        exec("sudo cat /etc/chrony/chrony.conf | grep -E '^pool' | awk '{print $2}'", $out);
        return $out[0];
    }

    public static function setNTPServer(string $server): bool
    {
        if ($server == self::getNTPServer()) {
            return false;
        }
        $oldNTP = Config::get('NTPServer');
        self::replaceTextInFile('/etc/chrony/chrony.conf', $oldNTP, $server);
        self::restartService("chrony");
        return true;
    }
    
    public static function getDNSServer(): string
    {
        $out = [];
        exec("sudo nmcli connection show ".Config::getStatic('ethConnectionName')." | grep ipv4.dns | awk '{print $2}'", $out);
        return $out[0];
    }

    public static function updateNetworkParams(string $newIP, string $newMask, string $newGateway, string $newDNS)
    {
        $oldIP = self::getLocalIP();
        $oldMask = self::calculateMask(self::getLocalMask());
        $oldGateway = self::getLocalGateway();
        $oldDNS = self::getDNSServer();

        if ($oldDNS != $newDNS) {
            exec("sudo nmcli connection modify ".Config::getStatic('ethConnectionName')." ipv4.dns $newDNS");
            exec("sudo nmcli device modify ".Config::getStatic('ethDeviceName')." ipv4.dns $newDNS");
        }
        if ($newIP != $oldIP || $newMask != $oldMask || $newGateway != $oldGateway) {
            $mask = self::calculateNonZeroValuesInMask($newMask);
            if ($mask < 1 || $mask > 32) {
                Logger::writeLog("Ошибка установки параметров сети. Маска задана неверно", "ERROR");
                return;
            }
            exec("sudo nmcli connection modify ".Config::getStatic('ethConnectionName')." ipv4.addresses $newIP/$mask");
            exec("sudo nmcli device modify ".Config::getStatic('ethDeviceName')." ipv4.addresses $newIP/$mask");
            exec("sudo nmcli connection modify ".Config::getStatic('ethConnectionName')." ipv4.gateway $newGateway");
            exec("sudo nmcli device modify ".Config::getStatic('ethDeviceName')." ipv4.gateway $newGateway");
        }
    }

    private static function replaceTextInFile(string $filename, string $strToReplace, string $replaceStr)
    {
        $cnt = str_replace($strToReplace, $replaceStr, file_get_contents($filename));
        self::writeToFile($filename, $cnt);
    }

    public static function writeToFile(string $filename, string $content)
    {
        exec("echo '$content' | sudo tee $filename");
    }

    public static function restartService(string $serviceName)
    {
        exec("sudo /etc/init.d/$serviceName restart");
    }

    public static function killProcess(string $processName)
    {
        $pids = self::getPIDs("$processName");
        foreach ($pids as $pid) {
            exec("sudo kill $pid");
        }
    }

    public static function startModbusGate()
    {        
        if (!SysUtils::isProcessRunning(Config::getStatic('modbusGatePath'))) {            
            $exec = 'sudo '.Config::getStatic('modbusGatePath').
                    ' -T '. Config::getStatic('defaulTty').
                    ' -S '. Config::get('modbusBaudRate').
                    ' -b '. Config::get('modbusNumBits').
                    ' -p '. substr(Config::get('modbusParity'), 0, 1).
                    ' -s '. Config::get('modbusStopBits').
                    ' -d '. Config::get('modbusReadDelay');
            exec($exec." > /dev/null &", $out);     
            sleep(1);
        }
    }
}
