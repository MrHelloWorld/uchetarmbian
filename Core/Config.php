<?php

namespace Core;

require_once 'DB.php';
require_once 'DBQuery.php';
//require 'SysUtils.php';

use Core\DBQuery;
use Core\SysUtils;
use Core\Database;

$staticCfg = require __DIR__.'/../config/config.php';

//uspd42

class Config
{
    public const SECTION_COMMON = 0;
    public const SECTION_LOCALMODBUS = 1;
    public const SECTION_REMOTEMODBUS = 2;
    public const SECTION_POLL = 3;
    public const SECTION_NETWORK = 4;

    public const TYPE_STRING = 0;
    public const TYPE_INT = 1;
    public const TYPE_IP = 2;

    public const MODBUS_LOCAL = '0';
    public const MODBUS_REMOTE = '1';

    private static $params = [];

    public static function set(string $param, string $value, int $section, int $type = self::TYPE_STRING): bool
    {
        @self::get(null);
        $query = new DBQuery(Database::get());

        if ($query->selectMax('config', 'id', ['key'=>"'$param'"]) == null) {
            $query->insert('config', ['key'=>$param, 'value'=>$value, 'section'=>$section, 'type'=>$type]);
            self::$params[$section][$param][0] = $value;
            return true;
        }
        $typ = self::$params[$section][$param][1];
        switch ($typ)
        {
            case self::TYPE_INT:
                if (!is_numeric($value)) {
                    echo "NOT AN INT";
                    return false;
                }
                break;

            case self::TYPE_IP:
                if (!@inet_pton($value)) {
                    echo "NOT AN IP ADDRESS: $param: $value";
                    return false;
                }
                break;
            default:
                break;
        }
        $query->sql("UPDATE config SET value='$value' WHERE key='$param'");
        self::$params[$section][$param][0] = $value;
        return true;
    }

    public static function check(string $param, string $value, int $section, int $type = self::TYPE_STRING): bool
    {
        @self::get(null);
        $query = new DBQuery(Database::get());

        if ($query->selectMax('config', 'id', ['key'=>"'$param'"]) == null) {
            return false;
        }
        $typ = self::$params[$section][$param][1];
        switch ($typ)
        {
            case self::TYPE_INT:
                if (!is_numeric($value)) {
                    return false;
                }
                break;

            case self::TYPE_IP:
                if (!@inet_pton($value)) {
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

    public static function get($param)
    {
        if (empty(self::$params)) {
            $query = new DBQuery(Database::get());
            $res = $query->selectAll('config');
            foreach($res as $row) {
                self::$params[$row['section']][$row['key']] = [$row['value'], $row['type']];
            }
        }
        foreach (self::$params as $value) {
            if (isset($value[$param]))
                return $value[$param][0];
        }
        return null;
        //return self::$params[$param];
    }

    public static function getAll()
    {
        //var_dump(self::$params);
        @self::get(null);
        return self::$params;
    }

    public static function getStatic(string $param)
    {
        global $staticCfg;
        return $staticCfg[$param];
    }

    public static function reset()
    {
        (new DBQuery(Database::get()))->sql('delete from config');

        Config::set('listenPort', '1711', self::SECTION_COMMON, self::TYPE_INT);
        Config::set('serverIP', '192.168.1.42', self::SECTION_COMMON, self::TYPE_IP);
        Config::set('serverPort', '1690', self::SECTION_COMMON, self::TYPE_INT);
        Config::set('socketTimeoutSec', '4', self::SECTION_COMMON, self::TYPE_INT);
        Config::set('debugLevel', 'DEBUG', self::SECTION_COMMON, self::TYPE_STRING);//NONE, ERROR, DEBUG
        Config::set('storageDepth', '30', self::SECTION_COMMON, self::TYPE_INT);
        Config::set('maxLogSize', '100', self::SECTION_COMMON, self::TYPE_INT);
        Config::set('recordsPerPage', '30', self::SECTION_COMMON, self::TYPE_INT);

        Config::set('modbusLocalType', '0', self::SECTION_LOCALMODBUS, self::TYPE_INT);
        Config::set('modbusBaudRate', '115200', self::SECTION_LOCALMODBUS, self::TYPE_INT);
        Config::set('modbusNumBits', '8', self::SECTION_LOCALMODBUS, self::TYPE_INT);
        Config::set('modbusParity', 'None', self::SECTION_LOCALMODBUS, self::TYPE_STRING);
        Config::set('modbusStopBits', '1', self::SECTION_LOCALMODBUS, self::TYPE_INT);
        Config::set('modbusReadDelay', '75', self::SECTION_LOCALMODBUS, self::TYPE_INT);

        Config::set('modbusRemoteType', '0', self::SECTION_REMOTEMODBUS, self::TYPE_INT);
        Config::set('modbusGateIP', '192.168.1.70', self::SECTION_REMOTEMODBUS, self::TYPE_IP);
        Config::set('modbusGatePort', '10001', self::SECTION_REMOTEMODBUS, self::TYPE_INT);

        Config::set('pollTime', '30', self::SECTION_POLL, self::TYPE_INT);
        Config::set('pollShift', '3', self::SECTION_POLL, self::TYPE_INT);

        self::updateNetworkParams();
//        Config::set('localIP', '192.168.1.70', self::SECTION_NETWORK, self::TYPE_IP);
//        Config::set('localMask', '255.255.255.0', self::SECTION_NETWORK, self::TYPE_STRING);
//        Config::set('localGateway', '192.168.1.70', self::SECTION_NETWORK, self::TYPE_IP);
//        Config::set('NTPServer', '192.168.1.170', self::SECTION_NETWORK, self::TYPE_IP);
    }

    public static function updateNetworkParams()
    {
        Config::set('localIP', SysUtils::getLocalIP(), self::SECTION_NETWORK, self::TYPE_IP);
        Config::set('localMask', SysUtils::calculateMask(SysUtils::getLocalMask()), self::SECTION_NETWORK, self::TYPE_STRING);
        Config::set('localGateway', SysUtils::getLocalGateway(), self::SECTION_NETWORK, self::TYPE_IP);
        Config::set('NTPServer', SysUtils::getNTPServer(), self::SECTION_NETWORK, self::TYPE_STRING);
        Config::set('DNSServer', SysUtils::getDNSServer(), self::SECTION_NETWORK, self::TYPE_IP);
    }
}