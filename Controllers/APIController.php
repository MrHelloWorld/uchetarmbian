<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of APIController
 *
 * @author m
 */
require 'AbstractPageController.php';
require __DIR__.'/../Core/DBQuery.php';
require __DIR__.'/../Core/DB.php';

use Core\Database;
use Core\DBQuery;


class APIController extends AbstractPageController
{
    protected $query;

    //put your code here
    public function init() 
    {
        parent::init();
        $this->query = new DBQuery(Database::get());
    }

    public function process() 
    {
        $this->proceedActions();
    }
    
    protected function getTimeAction() 
    {
        $dt = new DateTime();
        echo $dt->format("H:i:s");        
    }
    
    protected function getAllCountersAction()
    {
        echo json_encode($this->query->selectAll('pm130_counters'));
    }
    
    protected function getDataAction()
    {
        $from = Request::getGET('from', 'int');
        echo json_encode($this->query->selectLast('pm130_data', 'unix_timestamp', $from));        
    }
}
