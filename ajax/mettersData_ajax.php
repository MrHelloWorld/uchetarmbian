<?php

require __DIR__.'/../Core/Helper.php';
require __DIR__.'/../Core/Request.php';
require __DIR__.'/../Core/DBQuery.php';
require __DIR__.'/../Core/DB.php';

use Core\DBQuery;
use Core\Database;
use Core\Config;
use Core\Request;

session_start();
if (!isset($_SESSION['loggedIn']) ||
    $_SESSION['loggedIn'] == false
    )
{
    echo "Время авторизации истекло...";
    exit();
}
$query = new DBQuery(Database::get());

if (!Request::isSetGET('start') ||
    !Request::isSetGET('end') ||
    !Request::isSetGET('id') ||
    !Request::isSetGET('diff')
 ) {
    return 'ERROR';
}

$selectedID = Request::getGET('id', 'int');
$minDate = Request::getGET('start', 'int');
$maxDate = Request::getGET('end', 'int');
$currentPage = Request::getGET('page', 'int');
$isDiff = Request::getGET('diff', 'int');
$needDownload = Request::isSetGET('download');

if (!$currentPage) {
    $currentPage = 1;
}
    
//isset($_GET['page']) ? $_GET['page'] : 1;
//$allCounters = $query->selectAll('pm130_counters');
$rowsPerPage = Config::get('recordsPerPage');

if (!isset($selectedID)) {
    return require 'views/pm130data.view.php';
}
$where = [
    'idColumn' => 'pm130_id',
    'id' => $selectedID,
    'column' => 'unix_timestamp',
    'min' => $minDate,
    'max' => $maxDate,
    'limit' => $rowsPerPage,
    'offset' => ($currentPage - 1) * $rowsPerPage
];
$numRows = $query->numRowsBetweenForID('pm130_data', $where);
if (!$isDiff) {
    if ($needDownload) {
        unset($where['limit']);
        unset($where['offset']);
        $selectedCounter = $query->selectBetweenForIDGen('pm130_data', $where);
    } else {
        $selectedCounter = $query->selectBetweenForID('pm130_data', $where);
    }
} else {
    if ($needDownload) {
        $selectedCounter = $query->selectDataDiffBetweenForIDGen($selectedID, $minDate, $maxDate);
    } else {
        $selectedCounter = $query->selectDataDiffBetweenForID($selectedID, $minDate, $maxDate, $rowsPerPage, ($currentPage - 1) * $rowsPerPage);
    }
    $numRows--;
}
if ($needDownload)
{
    $metterName = $query->selectMax('pm130_counters', 'name', ['id' => $selectedID]);
    
    header("Content-Type: text/csv; charset=utf-8");
    header("Content-Disposition: attachment; filename=\"$metterName.csv\"");
    $output = fopen("php://output", "w");
    
    fputcsv($output, array_keys($selectedCounter->current()));
    foreach ($selectedCounter as $val)
    {
        //var_dump($val);
        fputcsv($output, $val);
    }
    fclose($output);
    die();
}
$numPages = ceil($numRows / $rowsPerPage);
$link = 'pm130data.php?id='.$selectedID;
    
$render = <<< __END
        <nav aria-lable="Data navigation">
        <ul class="pagination justify-content-center">
__END;

//Можно убрать не показывать если страниц меньше 10
if ($currentPage == 1) {
    $render .= '<li class="page-item disabled">';
} else {
    $render .= '<li class="page-item">';
}
$render .= '<a class="page-link" href="'.$link.'&page=1" onclick="updateData(this, event)">&laquo;</a></li>';

if ($numPages > 10)
{

    $min = 1;
    $max = 9;
    if ($currentPage - 4 > 0)
    {
        $min = $currentPage - 4;
        $max = 9 + $min - 1;
    }
    if ($currentPage + 4 > $numPages)
    {
        $min = $numPages-8;
        $max = $numPages;
    }
    for ($i=$min; $i<=$max; $i++)
    {
        if ($i == $currentPage) {
            $render .= '<li class="page-item active"> <a class="page-link" href="' . $link . '&page=' . $i . '"onclick="updateData(this, event)">' . $i . '</a>  </li>';
        } else {
            $render .= '<li class="page-item">       <a class="page-link" href="' . $link . '&page=' . $i . '"onclick="updateData(this, event)">' . $i . '</a>  </li>';
        }
    }
}else
{
    for ($i=1; $i<$numPages+1; $i++)
    {
        if ($i == $currentPage) {
            $render .= '<li class="page-item active">';
        } else {
            $render .= '<li class="page-item">';
        }
        $render.= '<a class="page-link" href="'.$link.'&page='.$i.'" onclick="updateData(this, event)">'.$i.'</a>';
        $render.= '</li>';
    }
}

if (!$numPages || $currentPage == $numPages) {
    $render .= "<li class='page-item disabled'>";
} else {
    $render .= "<li class='page-item'>";
}

$render .= "<a class='page-link' href='$link&page=$numPages' onclick='updateData(this, event)'>&raquo;</a>";
//$render .= "<li class='page-item "                .!$numPages || $currentPage == $numPages) ? 'disabled' : '"">;
//                            <a class="page-link" href="<?= $link.'&page='.$numPages">&raquo;</a>
$render .= '</li>';
//<!--<li class="page-item"> <a class="page-link">3</a></li>-->
$render .= '</ul>';
$render .= '</nav>';

$render .= \Core\HelpTo::returnHTMLTable($selectedCounter);
echo $render;