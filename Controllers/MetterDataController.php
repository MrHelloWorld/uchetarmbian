<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MetterDataController
 *
 * @author m
 */
require 'AbstractPageController.php';
require_once __DIR__.'/../Core/DB.php';
require_once __DIR__.'/../Core/DBQuery.php';
require_once __DIR__.'/../Core/Request.php';

use Core\DBQuery;
use Core\Database;
use Core\Request;

class MetterDataController extends AbstractPageController
{
    public function process() 
    {
        $query = new DBQuery(Database::get());
        $allCounters = $query->selectAll('pm130_counters');
        $selectedID = Request::getGET('id', 'int');
        
        $params = compact('allCounters', 'selectedID');
        $this->render('views/mettersData.view.php', $params);
    }
}
