<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SettingsController
 *
 * @author m
 */
require 'AbstractPageController.php';
require __DIR__.'/../Core/Helper.php';
require __DIR__.'/../Core/CronTask.php';
require __DIR__.'/../Core/Alias.php';
require __DIR__.'/../Core/Config.php';

use Core\SysUtils;
use Core\Config;
use Core\CronTask;
use Core\HelpTo;
use Core\Request;
use Core\Logger;

class SettingsController extends AbstractPageController
{
    private $params;
    
    //put your code here
    public function process()
    {
        //Config::reset();
        $this->proceedActions();
        Config::updateNetworkParams();
        $settings = Config::getAll();
        //var_dump($settings);
        $this->params['settings'] = $settings;
        $this->render('views/settings.index.view.php', $this->params);
    }

    protected function rebootAction()
    {
        system("(sleep 3 > /dev/null && sudo reboot) > /dev/null &");
        Request::redirect("index.php");
    }

    protected function updateCommonAction()
    {
        foreach ($_GET as $key => $value)
        {
            if ($key === 'action') {
                continue;
            }
            if ($key === 'debugLevel' && !in_array($value, ['ERROR', 'DEBUG', 'NONE'])) {
                $isError = true;
            } else {
                $isError = !Config::set($key, $value, Config::SECTION_COMMON);
            }
            if ($isError) {
                $this->params[$key] = ' alert alert-danger';
                $this->params['commonError'] = ' alert alert-danger';
                $this->params[$key.'_msg'] = "Неверное значение: $value";
            } else {
                $this->params[$key] = '';
                $this->params['commonError'] = '';
                $this->params[$key.'_msg'] = "";
            }
        }
    }
    
    protected function updateLocalModbusAction()
    {
        foreach ($_GET as $key => $value)
        {
            if ($key === 'action') {
                continue;
            }
            $isError = !Config::set($key, $value, Config::SECTION_LOCALMODBUS);
            if ($isError) {
                $this->params[$key] = ' alert alert-danger';
                $this->params['modbusError'] = ' alert alert-danger';
                $this->params[$key.'_msg'] = "Неверное значение: $value";
            } else {
                $this->params[$key] = '';
                $this->params[$key.'_msg'] = "";
            }
        }
        Config::set('modbusRemoteType', 0, Config::SECTION_REMOTEMODBUS);   
        SysUtils::killProcess(Config::getStatic('modbusGatePath'));
        SysUtils::startModbusGate();
    }
    
    protected function updateRemoteModbusAction()
    {
        foreach ($_GET as $key => $value)
        {
            if ($key === 'action') {
                continue;
            }
            $isError = !Config::set($key, $value, Config::SECTION_REMOTEMODBUS);
            if ($isError) {
                $this->params[$key] = ' alert alert-danger';
                $this->params['modbusError'] = ' alert alert-danger';
                $this->params[$key.'_msg'] = "Неверное значение: $value";
            } else {
                $this->params[$key] = '';
                $this->params[$key.'_msg'] = "";
            }
        }
        Config::set('modbusLocalType', 1, Config::SECTION_LOCALMODBUS);
    }
    
    protected function updatePollAction()
    {
        foreach ($_GET as $key => $value)
        {
            if ($key === 'action') {
                continue;
            }
            $isError = !Config::set($key, $value, Config::SECTION_POLL);
            if ($isError) {
                $this->params[$key] = ' alert alert-danger';
                $this->params['pollError'] = ' alert alert-danger';
                $this->params[$key.'_msg'] = "Неверное значение: $value";
            } else {
                $this->params[$key] = '';
                $this->params[$key.'_msg'] = "";
            }
        }
        if (!$isError) {
            $cron = CronTask::read('poll',$needCreate = false);
            if (!$cron) {
                Logger::writeLog("Ошибка изменения параметров опроса", $type = 'ERROR');
            } else {
                $cron->minutes = Config::get('pollShift')."-59/".Config::get('pollTime');
                $cron->write();
            }
        }
    }
    
    protected function updateNetworkAction()
    {
        foreach ($_GET as $key => $value)
        {
            if ($key === 'action') {
                continue;
            }
            $isError = !Config::check($key, $value, Config::SECTION_NETWORK);
            if ($isError) {
                $this->params[$key] = ' alert alert-danger';
                $this->params['networkError'] = ' alert alert-danger';
                $this->params[$key.'_msg'] = "Неверное значение: $value";
            } else {
                $this->params[$key] = '';
                $this->params[$key.'_msg'] = "";
            }
        }
        if (!$isError) {
            SysUtils::setNTPServer(Request::getGET('NTPServer', 'string'));
            Config::set('NTPServer', Request::getGET('NTPServer', 'string'), Config::SECTION_NETWORK);
            SysUtils::updateNetworkParams(
                    Request::getGET('localIP', 'string'),
                    Request::getGET('localMask', 'string'),
                    Request::getGET('localGateway', 'string'),
                    Request::getGET('DNSServer', 'string')
            );
        }
    }
}