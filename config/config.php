<?php

return [
    'dbPath' => '/uspd/db.db',//'/root/db.db',
    'logPath' => '/uspd/log.txt',
    'toGetFreeSpace' => '/root',
    'requestDataScript' => 'php -f /www/scripts/requestData.php',
    'daemonScript' => 'php -f /www/scripts/daemon.php',
    'ethDeviceName' => 'ens33',
    'ethConnectionName' => 'ETH',
    'modbusGatePath' => '/www/scripts/modbusgate',
    'defaulTty' => '/dev/ttyUSB0'
    ];