<?php
require __DIR__.'/../Controllers/APIController.php';

$api = new APIController($needLogin = false);
$api->init();
$api->process();
