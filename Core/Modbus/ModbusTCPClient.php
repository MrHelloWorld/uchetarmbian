<?php
namespace Core\Modbus;

require_once 'ModbusClient.php';
require_once __DIR__.'/../Helper.php';
require_once __DIR__.'/../Logger.php';

use Exception;
use Core\HelpTo;
use Core\Logger;

class ModbusTCPClient extends ModbusClient
{
    /**
     * MODBUS FRAME:
     *
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID    FUNCTION_CODE   DATA
     *      2b              2b        2b       1b            1b         (2b for start + 2bfor lenght) FOR READ_HOLDING
     */

    protected $ip;
    protected $port;
    protected $socket = null;
    protected $socketTimeout = null;
    protected $transactionID = 1;

    public function __construct($ip, $port, $deviceID, $debug = false)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->deviceID = $deviceID;
        $this->debug = $debug;
    }

    public function setDeviceID ($id)
    {
        $this->deviceID = $id;
    }

    public function connect(): bool
    {
        if (!$this->isConnected) {
            if ($this->debug) {
                echo "Connecting to $this->ip:$this->port... ";
            }
            $this->socket = stream_socket_client('tcp://'.$this->ip.':'.$this->port, $errno, $errstr);
            if (!$this->socket) {
                Logger::writeLog("Ошибка при подключении к $this->ip:$this->port", 'ERROR');
                echo "Ошибка при подключении к {$this->ip}: {$this->port}: $errno - $errstr";
                //die();
                return false;
            }
            if ($this->debug) {
                echo "Done\n";
            }
            if (!stream_set_timeout($this->socket, $this->socketTimeout) ){
                if ($this->debug) {
                    echo "Cant set timeout for socket\n";
                }
                Logger::writeLog(__FUNCTION__." Cant set timeout for socket.");
            }
            $this->isConnected = true;
            return true;
        }
        else {
            echo "Modbus Socket is already opened\n";
        }
    }

    public function isConnected(): bool
    {
        return $this->isConnected;
    }

    public function disconnect()
    {
        if ($this->isConnected) {
            $ret = fclose($this->socket);
            $this->socket = null;
            return $ret;
        }
    }

    public function setSocketTimout($timeout)
    {
        $this->socketTimeout = $timeout;
    }

    private function incTransactioID()
    {
        $this->transactionID = (($this->transactionID + 1) & 0xFF);
        $this->transactionID == 0 ? $this->transactionID++ : true;
    }

    public function readHoldingRegisters(int $startAddr, int $lenght)
    {
        if (!$this->isConnected) {
            throw new Exception("readHoldingRegisters: not connected");
        }
        $packet = new ModbusTCPPacket($this->transactionID);
        $packet->initToReadHoldingRegisters($this->deviceID, $startAddr, $lenght);
        
        $this->writePacketToSocket($packet);
        $reievedPacket = $this->readPacketFromSocket("reading $lenght bytes from [$startAddr]");
        
        if ($reievedPacket->getTransactionId() !== $this->transactionID) {
            Logger::writeLog("Incorrect answer transID after readHoldingRegisters($startAddr, $lenght)");
        }
        if (!$reievedPacket->isValid()) {
            Logger::writeLog("Incorrect response function code after readHoldingRegisters($startAddr, $lenght)");
        }
        $this->incTransactioID();

        return $reievedPacket;
    }

    public function writeSingleRegister(int $startAddr, int $value)
    {
        if (!$this->isConnected) {
            throw new Exception("writeSingleRegister: not connected");
        }
        $packet = new ModbusTCPPacket($this->transactionID);
        $packet->initToWriteSingleRegister($this->deviceID, $startAddr, $value);

        $this->writePacketToSocket($packet);
        $reievedPacket = $this->readPacketFromSocket("writing [$value]->[$startAddr]");
        $this->incTransactioID();
        
        return $reievedPacket;
    }

    public function writeMultipleRegisters(int $startAddr, array $values)
    {
        if (!$this->isConnected) {
            throw new Exception("writeMultipleRegisters: not connected");
        }
        $packet = new ModbusTCPPacket($this->transactionID);
        $packet->initToWriteMultipleRegisters($this->deviceID, $startAddr, $values);

        $this->writePacketToSocket($packet);
        $reievedPacket = $this->readPacketFromSocket("writing [$values[0]...]->[$startAddr]");
        $this->incTransactioID();
        
        return $reievedPacket;
    }

    private function readPacketFromSocket(string $errorMsg = ""): ModbusTCPPacket
    {
        $recieved = fread($this->socket, 1024);
        $error = "Error reading response after: ".$errorMsg;
        if (!$recieved) {
            Logger::writeLog($error);
            echo "$error\n";
            throw new Exception($error);
        }
        $recPacket =  new ModBusTCPPacket();
        $recPacket->initFromRecieve($recieved);
        $this->debug? HelpTo::echoStrHex("Recieved", $recPacket->getRawData()): true;
        
        return $recPacket;        
    }
    
    private function writePacketToSocket(ModbusTCPPacket $packet)
    {
        fwrite($this->socket, $packet->getRawData());
        $this->debug? HelpTo::echoStrHex("Sent    ", $packet->getRawData()): true;
    }
}

