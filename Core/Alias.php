<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

class Alias
{    
    CONST alias = [
        'listenPort' => "Локальный порт",
        'serverIP' => "IP адрес сервера",
        'serverPort' => "Порт сервера",
        'socketTimeoutSec' => "Таймаут сокета, сек",
        'debugLevel' => "Тип журнала",
        'pollTime' => "Интервал опроса, мин",  
        'storageDepth' => "Глубина хранения данных, дней",  
        'maxLogSize' => "Максимальный размер лога, КБ",  
        'id' => 'Номер',
        'recordsPerPage' => 'Данных на странице',
        'pollShift' => 'Смещение опроса, мин',
        'modbusLocalType' => 'Интерфейс Modbus TCP',
        'modbusRemoteType' => 'Интерфейс Modbus TCP',
        'modbusGateIP' => 'Адрес ModbusTCP шлюза',
        'modbusGatePort' => 'Порт ModbusTCP шлюза',
        'modbusBaudRate' => 'Скорость порта, бит/с',
        'modbusNumBits' => 'Бит данных',
        'modbusParity' => 'Четность',
        'modbusStopBits' => 'Стоповых бит',
        'modbusReadDelay' => 'Пауза перед чтением из порта, мс',
        'localIP' => 'Локальный IP адес',
        'localMask' => 'Маска сети',
        'localGateway' => 'Шлюз',
        'NTPServer' => 'Сервер времени',
        'DNSServer' => 'Сервер DNS'
    ];


    public static function get($name)
    {
        //echo $name;
        if (array_key_exists($name, self::alias))
            return self::alias[$name];
        else
            return $name;
    }
    
}