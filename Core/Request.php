<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

/**
 * Description of Request
 *
 * @author Serg
 */
class Request
{
    public static function isSetGET($param)
    {
        return isset($_GET[$param]);
    }
    
    public static function isSetPOST($param)
    {
        return isset($_POST[$param]);
    }
    
    public static function getGET($param, $type)
    {
        switch ($type)
        {
            case 'int':
                $ret = filter_input(INPUT_GET, $param, FILTER_SANITIZE_NUMBER_INT);
                break;
            case 'string':
                $ret = filter_input(INPUT_GET, $param, FILTER_SANITIZE_STRING);
                break;
        }        
        return $ret;
    }
    
    public static function getPOST($param, $type)
    {
        switch ($type)
        {
            case 'int':
                $ret = filter_input(INPUT_POST, $param, FILTER_SANITIZE_NUMBER_INT);
                break;
            case 'string':
                $ret = filter_input(INPUT_POST, $param, FILTER_SANITIZE_STRING);
                break;
        }        
        return $ret;        
    }
    
    public static function redirect($to)
    {
        header ("Location: $to");
        exit();
    }
}
