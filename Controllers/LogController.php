<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LogController
 *
 * @author m
 */
require __DIR__.'/../Core/Logger.php';

require 'AbstractPageController.php';
//require __DIR__.'/../Core/Helper.php';

use Core\Logger;
use Core\HelpTo;

class LogController extends AbstractPageController
{
    public function process() 
    {
        $render = Logger::getRender();
        HelpTo::render($render);
    }

}
