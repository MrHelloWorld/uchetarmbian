<?php
require __DIR__.'/../Core/Daemon.php';
require __DIR__.'/../Core/SysUtils.php';

use Core\Daemon;
use Core\SysUtils;
use Core\Server\ResponseServerService;
use Core\Config;
use Core\Logger;

set_time_limit(0);

$daemonScript = Config::getStatic('daemonScript');
$isDebug = in_array('debug', $argv);

if (in_array('stop', $argv)) {
    SysUtils::killProcess($daemonScript);
    die();
}
if (count(SysUtils::getPIDs($daemonScript)) > 1) {    
    Logger::writeLog("Сервер ответа уже запущен...", 'ERROR');
    echo "Daemon is already running...\n";    
    die();
}

$daemon = new Daemon();
$localIP = SysUtils::getLocalIP();
$localPort =  Config::get('listenPort'); 
$serverPort = Config::get('serverPort');        
$serverIP = Config::get('serverIP');        
        
$service = new ResponseServerService($localIP, $localPort, $serverIP, $serverPort, $isDebug);
$daemon->addService($service);
if (!$daemon->init()) {
    die('Cant init');
}
$daemon->run();
