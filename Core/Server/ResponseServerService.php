<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Server;

require 'AbstractService.php';
require __DIR__.'/../Logger.php';
//require __DIR__.'/../Helper.php';
require 'ServerPacketsProvider.php';

use Core\HelpTo;
use Core\Logger;

/**
 * Description of ResponseServerService
 *
 * @author Serg
 */
class ResponseServerService extends AbstractService
{
    private $socketWaitNs = 50000000;

    public function __construct($localIP, $localPort, $serverIP, $serverPort, $debug = false) 
    {
        $this->localIP = $localIP;
        $this->localPort = $localPort;
        $this->serverIP = $serverIP;
        $this->serverPort = $serverPort;          
        $this->isReadyToRun = false;
        $this->socket = null;
        $this->debug = $debug;
    }
    
    public function init()
    {
        $this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        if (!$this->socket) {
            Logger::writeLog("Error creating recieve ResponseServerService socket!", 'ERROR');
            return false;
        }

        if (!socket_bind($this->socket, $this->localIP, $this->localPort)) {
            Logger::writeLog("Can not bind recieve socket on {$this->localIP}:{$this->localPort} in ResponseServerService!", 'ERROR');
            $this->socket = null;
            return false;
        }
        $this->isReadyToRun = false;
        return true;
    }

    public function clean()
    {
        $this->isReadyToRun = false;
        if (!$this->socket) {
            socket_close($this->socket);
        }
    }

    public function doWork()
    {
        if ($this->debug) {
            echo ".";
        }    
        $recievedData = $this->recieveDataPacket();
        if ($recievedData == null) {
            return;
        }
        $packetsProvider = new ServerPacketsProvider($recievedData, $this->debug);
        $responsePacket = $packetsProvider->createResponsePacket();
        $responsePacket->send($this->socket, $this->serverIP, $this->serverPort);
    }
    
    public function recieveDataPacket()
    {
        $buf = '';
        $fromIP = '';
        $fromPort = 0;
        socket_recvfrom($this->socket, $buf, 512, MSG_DONTWAIT, $fromIP, $fromPort);        
        $len = strlen($buf);        
        if ($len > 0 ) {
            if ($this->debug) {
                echo "Recieved from $fromIP $len bytes: ";
                HelpTo::echoHex($buf);
            }
            return $buf;
        }
        time_nanosleep(0, $this->socketWaitNs);
        return null;
    }
    
}
