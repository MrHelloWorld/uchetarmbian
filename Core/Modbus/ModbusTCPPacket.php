<?php

namespace Core\Modbus;

require 'ModbusRTUPacket.php';
require_once __DIR__.'/../Helper.php';

use Core\HelpTo;
    /**
     * MODBUS SEND FRAME:
     *                  Header                                  PDU             Starting Register     Number of regs to read
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE           START_REG           REGS_NUM
     *      2b              2b        2b       1b               1b                     2b                  2b
     *
     *
     * MODBUS RECIEVE FRAME:
     *                  Header                                  PDU
     * TRANSACTION_ID  PROTOCOL_ID  LENGHT  UNIT_ID        FUNCTION_CODE   REC_LENGHT       DATA
     *      2b              2b        2b       1b               1b            2b         REC_LENGHT
     */

class ModbusTCPPacket extends ModbusRTUPacket
{
    protected $transactionID = 0;
    protected $protocolID = 0;
    protected $packetLenght = 0;

    public function __construct(int $transactionID = 0)
    {
        parent::__construct();
        $this->transactionID = $transactionID;
    }
    
    public function initFromRecieve(string $recievedData)
    {
        $this->rawData = $recievedData;
        $header = substr($recievedData, 0, 7);
        $PDU = substr($recievedData, 7, strlen($recievedData));

        $tmp = unpack("n3/cID", $header);
        $this->transactionID = $tmp[1];
        $this->protocolID = $tmp[2];
        $this->packetLenght = $tmp[3];
        $this->deviceID = $tmp['ID'];

        $tmp = unpack("c*", substr($PDU, 0, 2));
        $this->functionCode = isset($tmp[1]) ? $tmp[1]: 0;
        $this->recievedDataLenght = isset($tmp[2]) ? $tmp[2]: 0;
        $this->recievedData = substr($PDU, 2, strlen($PDU));
    }

    public function initToReadHoldingRegisters(int $deviceID, int $startAddr, int $lenght)
    {
        parent::initToReadHoldingRegisters($deviceID, $startAddr, $lenght);
        $this->packetLenght = 0x06;
        $this->rawData = pack("nnn", $this->transactionID, ModbusRTUPacket::PROTOCOL_ID, $this->packetLenght).$this->rawData;
    }

    public function initToWriteSingleRegister(int $deviceID, int $startAddr, int $value)
    {
        parent::initToWriteSingleRegister($deviceID, $startAddr, $value);
        $this->packetLenght = 0x06;
        $this->rawData = pack("nnn", $this->transactionID, ModbusRTUPacket::PROTOCOL_ID, $this->packetLenght).$this->rawData;
    }

    public function initToWriteMultipleRegisters(int $deviceID, int $startAddr, array $values)
    {
        parent::initToWriteMultipleRegisters($deviceID, $startAddr, $values);
        $this->packetLenght = 0x07 + 2*count($values);
        $this->rawData = pack("nnn", $this->transactionID, ModbusRTUPacket::PROTOCOL_ID, $this->packetLenght).$this->rawData;
    }

    public function printPacket()
    {
        echo 'TransactionID: '.$this->transactionID.'<br>\n';
        echo 'ProtocolID: '.$this->protocolID.'<br>\n';
        echo 'PacketLenght: '.$this->packetLenght.'<br>\n';
        echo 'DeviceID: '.$this->deviceID.'<br>\n';
        echo 'Function Code: '.$this->functionCode.'<br>\n';
        echo 'Recieved Lenght: '.$this->recievedDataLenght.'<br>\n';
        echo 'Recieved Data: '; HelpTo::echoHex($this->recievedData);
        echo "<br>\n";
    }

    public function getData($dataType)
    {
        $ret = [0];

        switch ($dataType)
        {
            case 'UINT16':
                $format = "n*";
                $ret = unpack($format, $this->recievedData);
                break;

            case 'INT16':
                $format = "s*";
                $ret = unpack($format, $this->recievedData);
                break;

            case 'UINT32':
                $format = "n*";
                $arr = unpack($format, $this->recievedData);
                
                for ($i=0; $i<count($arr); $i+=2) {
                    $ret[] = $arr[$i+1] + $arr[$i+2] * 65536;   
                }                
                //$ret = $arr[1] + $arr[2] * 65536;                
                break;

            default:
                echo 'UNKNOW DATA FORMAT';
                break;
        }
        if (count($ret) > 2) {
            return $ret;
        }           
        return $ret[1];
    }

    public function getTransactionId(): int
    {
        return $this->transactionID;
    }
}
