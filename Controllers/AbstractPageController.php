<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require __DIR__.'/../Core/Request.php';

use Core\Request;

abstract class AbstractPageController
{
    public $isLoggedIn;
    private $needLogin;

    abstract public function process();

    public function __construct(bool $needLogin = true)
    {
        $this->needLogin = $needLogin;
    }

    public function init()
    {
        if ($this->needLogin) {
            session_start();
            if ($_SESSION['loggedIn'] == false) {
                Request::redirect('login.php');
            } else {
                $this->isLoggedIn = true;
            }
        }
    }

    protected function render(string $view, array $params)
    {        
        extract($params);
        include($view);
    }

    protected function proceedActions()
    {
        $class = new static;
        
        if (!Request::isSetGET('action'))
            return;
        $action = Request::getGET('action', 'string');
        if (method_exists($class, $action."Action")) {
            $this->{$action."Action"}();
        } else {
            echo "No method implements for $action";
            //die();
        }
    }
}
