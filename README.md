Комментарии для установки:

1. Rotation logs?
2. Timezone:
	rm /etc/localtime
	ln -s /usr/share/zpneinfo/Etc/GMT... /etc/localtime
3. Sync time: chrony is now. NTPd?
	chronyc add server *
	chronyc delete *
	sync time now: sudo chronyc -a makestep
4. php.ini:
		max_execution_time
5. Add user www-data to /etc/sudoers
6. Rename connection to ETH
	nmcli connection modify __OLD__ con-name ETH
7. Leave only one line starting with 'pool'	in /etc/chrony/chrony.conf
8. ln -s log to /www/
9. Add daemon script  to /etc/rc.local (without sudo)
10. Add /www/scripts/heartbeat 68 to /etc/rc.local
11. Deal with RTC:
		sudo echo ds3231 0x68 > /sys/class/i2c-adapter/i2c-0/new_device
		sudo hwclock -s -f /dev/rtc1

Maybe need to manualy set time to RTC1 after correcting system time?

