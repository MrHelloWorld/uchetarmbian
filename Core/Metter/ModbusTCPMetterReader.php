<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Metter;

require 'MetterReaderInterface.php';
require_once __DIR__.'/../Modbus/ModbusTCPClient.php';

/*
 * class ModbusTCPCounterReader
 * 
 * 
 * @property  ModbusTCPClient $modbusTCPClient
 */
abstract class ModbusTCPMetterReader implements MetterReaderInterface
{    
    protected $modbusTCPClient;    
    protected $isConnected;

    public function __construct($modbusTCPClient) 
    {
        $this->modbusTCPClient = $modbusTCPClient;
    }
    
    public function isConnected() 
    {
        return $this->modbusTCPClient->isConnected();
    }

}
