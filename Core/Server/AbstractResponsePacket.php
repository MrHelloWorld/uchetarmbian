<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Server;

/**
 * Description of AbstractResponsePacket
 *
 * @author Serg
 */
abstract class AbstractResponsePacket 
{
    protected $rawData;
    
    public function __construct($packet)
    {
        $this->rawData = $packet;
    }
    
    abstract public function create();
    abstract public function send($socket, $serverIP, $serverPort);
}
