<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

require 'Config.php';
require 'SysUtils.php';
require 'Helper.php';
require 'Logger.php';
require 'Modbus/ModbusTCPClient.php';
require 'Modbus/ModbusTCPPacket.php';
require 'Metter/PM130ModbusTCPReader.php';
require 'Metter/PM130Metter.php';

use Core\SysUtils;
use Core\Database;
use Core\DBQuery;
use Core\HelpTo;
use Core\Config;
use DateTime;
use DateInterval;
use Core\Modbus\ModbusTCPClient;
use Core\Metter\PM130ModbusTCPReader;
use Core\Metter\PM130Metter;
use Exception;
/**
 * Description of GoRequestData
 *
 * @author Serg
 */
class GoRequestData
{
    private $debug;

    public function __construct($debug = false)
    {
        $this->debug = $debug;
    }

    private function truncateDataBase()
    {
        $query = new DBQuery(Database::get());
        $dt = new DateTime();
        $dt->sub(new DateInterval('P'   .Config::get('storageDepth').'D'));
        $query->deleteLessThan('pm130_data', 'unix_timestamp', $dt->format('U'));
    }

    private function checkModbusRTUGate()
    {
       if (!SysUtils::isProcessRunning('modbusgate')) {
            Logger::writeLog("Локальный ModbusTCP шлюз отключен. Перезапуск...", 'ERROR');
            SysUtils::startModbusGate();
        } 
        sleep(1);
    }
    
    public function init()
    {
        if (count(SysUtils::getPIDs(Config::getStatic('requestDataScript'))) > 1) {
            Logger::writeLog("Already polling data...");
            echo 'Already polling data...';
            die();
        }
        Logger::truncateLogFile();
        $this->truncateDataBase();
        if (Config::get('modbusLocalType') == Config::MODBUS_LOCAL)
            $this->checkModbusRTUGate();
    }

    public function run()
    {
        $query = new DBQuery(Database::get());
        $AllCounters = $query->selectAll('pm130_counters');

        if (Config::get('modbusLocalType') == Config::MODBUS_LOCAL) {
            $ip = '127.0.0.1';
            $port = 10001;
        } else {
            $ip = Config::get('modbusGateIP');
            $port = Config::get('modbusGatePort');
        }
        $render = "<h1 class=\"text-center\"> Данные добавлены: </h1>";
        foreach ($AllCounters as $counter) {
            $modbusId = $counter['device_id'];
            $dbID = $counter['id'];
            $numAddedLines = 0;

            $tcpClient = new ModbusTCPClient($ip, $port, $modbusId, $this->debug);
            $tcpClient->setSocketTimout(Config::get('socketTimeoutSec'));
            $pm130Reader = new PM130ModbusTCPReader($tcpClient);

            $pm130Metter = new PM130Metter($pm130Reader);
            $pm130Metter->setMaxUnsync(3);
            
            if ($pm130Metter->open() == false) {
                continue;
            }
            $lastTimeStamp = $query->selectMax('pm130_data', 'unix_timestamp', ['pm130_id'=>$dbID]);
            if ($this->debug) {
                echo "Oldest data timestamp in DB: $lastTimeStamp\n";
            }
            Logger::writeLog("Start requesting: MetterID=$dbID, ModbusID={$modbusId} from: ".HelpTo::printUnixTime($lastTimeStamp));
            //echo "Request: $dbID @ $modbusId from ".HelpTo::printUnixTime($lastTimeStamp)."\n";
            
            /*$res = $pm130Metter->getA2R2($lastTimeStamp );

            if ($res == null) {
                //echo "NULL 1";
                Logger::writeLog("Got null from generator", 'ERROR');
                continue;
            }*/
            $numRetries = 3;
            while ($numRetries > 0) {
                if ($this->debug) {
                    echo "Starting requesting data from ".HelpTo::printUnixTime($lastTimeStamp)."\n";
                }                
                $res = $pm130Metter->getA2R2($lastTimeStamp );
                try {
                    foreach ($res as $value) {
                        if ($value == null) {
                            Logger::writeLog("Got null from generator while iterating", 'ERROR');
                            //echo "NULL FROM GEN";
                            $numRetries = 0;
                            continue;
                        }
                        $value['pm130_id'] = $dbID;
                        if ($this->debug) {
                            echo "Recieved [{$value['value1']}, {$value['value2']}, {$value['value3']}, {$value['value4']}] at ".HelpTo::printUnixTime($value['unix_timestamp'])."\n\n";
                        }
                        $query->insert("pm130_data", $value);
                        $numAddedLines++;
                    }                
                    $corrected = $pm130Metter->correctMetterTime();
                    break;
                } catch (Exception $exc) {
                    $numRetries--;
                    $msg = $exc->getMessage();
                    Logger::writeLog("Ошибка при сеансе связи со счетчиком '{$counter['name']}. $msg. Повторов $numRetries");                                        
                }
            }
            if ($numRetries == 0) {                
                Logger::writeLog("Ошибка во время сеанса связи со счетчиком '{$counter['name']}", 'ERROR');
                $pm130Metter->close();
                continue;
            }
            if ($corrected != null) {
                Logger::writeLog("Correcting metter time: from {$corrected['oldTime']} to {$corrected['newTime']}", 'ERROR');
                if ($this->debug) {
                    echo "Correcting metter time: from {$corrected['oldTime']} to {$corrected['newTime']}\n";
                }
            }
            $render .= "<h6 class=\"text-center\"> {$counter['name']}: <b>$numAddedLines</b> строк добавлено </h6 >";
            $render .= "<br>";
            //break;

            $pm130Metter->close();
            sleep(1);
        }
        return $render;
    }
}