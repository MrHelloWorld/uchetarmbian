<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginController
 *
 * @author m
 */
require 'AbstractPageController.php';
//require __DIR__.'/../Core/Request.php';

use Core\Request;

class LoginController extends AbstractPageController
{
    public function process()
    {
        session_start();
        if (Request::isSetGET('logout')) {
            session_destroy();
            Request::redirect("login.php");
        }
        if (Request::getPOST('login', 'string') === 'root' &&
            md5(Request::getPOST('password', 'string')) === 'a1d0c6e83f027327d8461063f4ac58a6') {
            $_SESSION['loggedIn'] = true;
            Request::redirect("index.php");
        }
        if (Request::isSetPOST('login'))
            $loginFail = true;
        else
            $loginFail = false;
        $this->render('views/login.view.php', compact('loginFail'));
    }
}
