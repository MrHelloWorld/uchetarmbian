<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MettersController
 *
 * @author m
 */
require 'AbstractPageController.php';
require_once __DIR__.'/../Core/DB.php';
require_once __DIR__.'/../Core/DBQuery.php';
require_once __DIR__.'/../Core/Request.php';
require 'Core/Helper.php';

use Core\Request;
use Core\Database;
use Core\DBQuery;

class MettersController extends AbstractPageController
{
    private $query;
    //put your code here
    
    public function init() 
    {
        parent::init();
        $this->query = new DBQuery(Database::get());
    }
    
    public function process() 
    {
        $this->proceedActions();
        $countersTable = $this->query->selectAll('pm130_counters');
        $params = compact('countersTable');
        $this->render('views/metters.view.php', $params);
    }
    
    public function deleteAction()
    {
        $id = Request::getGET('id', 'int');
        $this->query->deleteByID('pm130_counters', $id);
        $this->query->deleteByColumn('pm130_data', 'pm130_id', $id);        
    }
    
    public function addAction()
    {
        $modbusId = Request::getGET('modbus_id', 'int');
        $name = Request::getGET('name', 'string');

        $val = ['device_id' => $modbusId,
                'name' => $name];
        $this->query->insert('pm130_counters', $val);
    }
    
    public function updateAction()
    {
        $id = Request::getGET('id', 'int');
        $row = Request::getGET('row', 'string');
        $value = Request::getGET('value', 'string');    
        $this->query->update('pm130_counters', $id, $row, $value);
        Request::redirect("metters.php");
    }
}
