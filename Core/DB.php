<?php

namespace Core;

require_once 'Config.php';

use PDO;
use PDOException;
use Core\CronTask;
use Core\Config;

class Database
{
    /** @var PDO  */
    public $pdo;
    private static $db = null;

    public function __construct()
    {

    }

    private function open($needInit)
    {
        $dbName = Config::getStatic('dbPath');

        //die();
        if (PHP_OS == "WINNT")
            $dbName = 'd:/db.db';
        try
        {
            //echo __DIR__;
            //echo $dbName;
            $this->pdo = new PDO('sqlite:'.$dbName);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $ex)
        {
            echo " Error opening DB: ".$ex->getMessage();
            return false;
        }
        return ($needInit? $this->initClear() : false);
    }

    public static function get($needInit = false)
    {
        if (self::$db === null) {
            $db = new Database;
            $db->open($needInit);
            return $db;
        }
        else {
            return $db;
        }
    }

    private function initClear()
    {
        $sqls = [
            'DROP TABLE IF EXISTS counters;',
            'CREATE TABLE counters (
                id INTEGER  PRIMARY KEY AUTOINCREMENT,
                counter_type_id INTEGER,
                name TEXT
             )'];
//        array_push($sqls, 'DROP TABLE IF EXISTS modbus_map');
//        array_push($sqls,
//                'CREATE TABLE modbus_map
//                        (
//                            id INTEGER PRIMARY KEY AUTOINCREMENT,
//                            counter_type_id INTEGER,
//                            name TEXT,
//                            address INTEGER,
//                            data_type TEXT,
//                            data_size INTEGER
//                  )');
//        array_push($sqls,
//                "INSERT INTO modbus_map (counter_type_id, name, address, data_type, data_size) VALUES
//                        (
//                            1,
//                            'Serial Number',
//                            46080,
//                            'UINT32',
//                            2
//                        )");
//        array_push($sqls,
//                "INSERT INTO modbus_map (counter_type_id, name, address, data_type, data_size) VALUES
//                        (
//                            1,
//                            'Seconds',
//                            4352,
//                            'UINT16',
//                            1
//                        )");
//        array_push($sqls,
//                "INSERT INTO modbus_map (counter_type_id, name, address, data_type, data_size) VALUES
//                        (
//                            2,
//                            'Minutes',
//                            4353,
//                            'UINT16',
//                            1
//                        )");
//        array_push($sqls,
//                "INSERT INTO modbus_map (counter_type_id, name, address, data_type, data_size) VALUES
//                        (
//                            1,
//                            'Hours',
//                            4354,
//                            'UINT16',
//                            1
//                        )");
//        array_push($sqls, 'DROP TABLE IF EXISTS modbus_poll_templates');
//        array_push($sqls,
//                'CREATE TABLE modbus_poll_templates
//                        (
//                            id INTEGER PRIMARY KEY AUTOINCREMENT,
//                            name TEXT,
//                            modbus_map_id INEGER,
//                            interval INTEGER
//                )');
//        array_push($sqls, "
//                INSERT INTO modbus_poll_templates (name, modbus_map_id, interval) VALUES
//                        (
//                            'Параметры опроса PM130 Ввод',
//                            1,
//                            60
//                        )
//                ");
//        array_push($sqls, "
//                INSERT INTO modbus_poll_templates (name, modbus_map_id, interval) VALUES
//                        (
//                            'Параметры опроса PM130 Ввод',
//                            1,
//                            60
//                        )
//                ");
//        array_push($sqls, "DROP TABLE IF EXISTS modbus_poll;");
//        array_push($sqls, "
//                CREATE TABLE modbus_poll
//                        (
//                            id INTEGER PRIMARY KEY AUTOINCREMENT,
//                            modbus_poll_template_id INTEGER,
//                            counter_id INTEGER,
//                            name TEXT
//                        )
//                ");
//        array_push($sqls,"
//                INSERT INTO modbus_poll (modbus_poll_template_id, counter_id, name) VALUES
//                        (
//                            1,
//                            1,
//                            'Набор опроса'
//                        )
//            ");
//        array_push($sqls, 'DROP TABLE IF EXISTS counter_types');
//        array_push($sqls,
//                'CREATE TABLE counter_types
//                        (
//                            id INTEGER PRIMARY KEY AUTOINCREMENT,
//                            name TEXT,
//                            protocol TEXT)'
//                    );
//        array_push($sqls, "INSERT INTO counter_types (name, protocol) VALUES ('SATEC PM-130EH PLUS', 'Modbus')");
//        array_push($sqls, "INSERT INTO counter_types (name, protocol) VALUES ('SATEC PM-130EH', 'Modbus')");
//
//        array_push($sqls, "DROP TABLE IF EXISTS meter_data");
        array_push($sqls, "DROP TABLE IF EXISTS pm130_counters");
        array_push($sqls, "
                    CREATE TABLE pm130_counters
                        (
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            device_id INTEGER,
                            name TEXT
                        )
                    ");
        array_push($sqls, "
                    INSERT INTO pm130_counters (device_id, name) VALUES
                            (
                                1,
                                'Ввод Т1'
                            )
                    ");
        array_push($sqls, "
                    INSERT INTO pm130_counters (device_id, name) VALUES
                            (
                                2,
                                'Ввод Т2'
                            )
                    ");
        array_push($sqls, "DROP TABLE IF EXISTS pm130_data");
        array_push($sqls, "
                    CREATE TABLE pm130_data
                        (
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            pm130_id INTEGER,
                            value1 INTEGER,
                            value2 INTEGER,
                            value3 INTEGER,
                            value4 INTEGER,
                            string_timestamp TEXT,
                            unix_timestamp INTEGER
                        )
                    ");
        array_push($sqls, "DROP TABLE IF EXISTS config");
        array_push($sqls, "
                    CREATE TABLE config
                        (
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            key TEXT,
                            value TEXT,
                            section INTEGER DEFAULT 0,
                            type INTEGER DEFAULT 0
                        )
                    ");

        array_push($sqls, "CREATE INDEX IF NOT EXISTS idx_data ON pm130_data(unix_timestamp)");

        foreach ($sqls as $sql)
        {
            //echo $sql."<br>";
            try
            {
                $this->pdo->exec($sql);
            } catch (PDOException $e) {
                echo "Error while Init DB: {$e->getMessage()}<br>";
                echo "SQL String: {$sql}<br>";
                echo '<br>';
            }

        }
        Config::reset();

        $task = CronTask::get(0);
        $task->minutes = Config::get('pollShift')."-59/".Config::get('pollTime');
        $task->set(0);
    }
}




