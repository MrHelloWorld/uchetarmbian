<!doctipe HTML>

<html lang="ru">
    <title>Metters</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/my.css">
    <script src="../../js/my.js" type="text/javascript"></script>
    <script src="../../js/jquery-3.4.1.min.js" type="text/javascript"></script>
    <body>
        <div class="container p-0 border">
            <div class="row">
                <div class="col">
            
                    <div id="loading" class="text-center fixed-bottom"><h1>LOADING...</h1></div>
                    <script>
                        $(function()
                        {
                            jQuery('#loading').hide();
                        });
                    </script>
            <?php require 'nav.view.php';
            
